<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Activitylog\Traits\LogsActivity;

use App\Medium;


/**
 * Class ProductCategory
 * @package Modules\Product\Entities
 * @version September 6, 2017, 8:31 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection Product
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection ProductsProductCategory
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property string name
 * @property string description
 * @property integer parent_id
 * @property integer parent_parent_id
 * @property string image
 * @property string icon
 */
class ProductCategory extends Model {
    use SoftDeletes;
	use LogsActivity;

    public $table = 'product_categories';


    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'parent_id',
        'parent_parent_id',
        'price',
        // 'image',
        // 'icon'
    ];

	protected static $logFillable = true;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'parent_id' => 'integer',
        'parent_parent_id' => 'integer',
        // 'image' => 'string',
        // 'icon' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(\Modules\Product\Entities\Product::class);
    }
    public function catpost()
    {
        return $this->hasMany(\Modules\Catpost\Entities\catpost::class);
    }
    public function fav() {
        return $this->hasMany(\Modules\Favorite\Entities\favorite::class);
    }
    public function paylist() {
        return $this->hasMany(\Modules\Paylist\Entities\paylist::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productsProductCategories()
    {
        return $this->hasMany(\Modules\Product\Entities\ProductsProductCategory::class);
    }

	public function media(){
        return $this->morphMany(Medium::class, 'mediumable');
    }

	public function getIconAttribute(){
		return $this->media()->where('manner', 'icon')->first();
	}

	public function getImageAttribute(){
		return $this->media()->where('manner', 'image')->first();
	}

	public function getDescriptionForEvent(string $eventName): string {
		switch ($eventName) {
			case 'created':
				return "ثبت دسته بندی جدید (محصولات)";
				break;

			case 'updated':
				return "ویرایش دسته بندی (محصولات)";
				break;

			case 'deleted':
				return "حذف دسته بندی (محصولات)";
				break;

			default:
				return '';
				break;
		}
    }
}
