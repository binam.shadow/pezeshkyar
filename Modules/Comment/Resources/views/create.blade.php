@extends('adminbsb.layouts.master')

@section('title')
    @lang('comment::default.create')
@endsection

@section('styles')
    @include('comment::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('comment::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('comment::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('comment.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="description" value="{{ old('description') }}" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.description')</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('comment::partials._script')
@endsection
