<?php
namespace Modules\Product\Config;
use App\Menu;
use App\Permission;
use App\Role;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'Product';
    public $fa_name = 'محصول';

    /**
     *
     */
    public function setup(){
        $role = Role::where('name', 'super-admin')->first();

        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.list'], $permissions);

		$role->attachPermission($create);
		$role->attachPermission($edit);
		$role->attachPermission($list);

        $en_name = 'Category';
        $fa_name = 'دسته بندی';
        $permissions = [
            'display_name' => 'ایجاد ' . $fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($en_name) . '.list'], $permissions);

		$role->attachPermission($create);
		$role->attachPermission($edit);
		$role->attachPermission($list);

        $menu1 = [
            'display_name'  => $fa_name,
            'icon'          => 'view_headline'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($en_name)], $menu1);

        $menu2 = [
            'display_name' => 'ایجاد ' . $fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($en_name . '.create'),
        ], $menu2);

        $menu3 = [
            'display_name' => 'آرشیو ' . $fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($en_name . '.list'),
        ], $menu3);

        Artisan::call('module:migrate', ['module' => $this->en_name]);

    }

    /**
     * @throws \Exception
     */
    public function remove(){

        Permission::where('name', 'like', strtolower($this->en_name).'%')->delete();

        Role::where(['name' => strtolower($this->en_name)])->delete();

        $en_name = 'Category';
        Menu::where('name', 'like', strtolower($en_name).'%')->delete();
        Menu::where('name', 'like', strtolower($this->en_name).'%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => $this->en_name ]);
    }
}
