<?php

namespace Modules\Note\Entities;

use Illuminate\Database\Eloquent\Model;

class note extends Model
{
    protected $fillable = [
        'name',
        'description',
        'user_id',
        'post_id'
    ];


    public function post()
    {
        return $this->belongsTo(\Modules\Tabpost\Entities\tabpost::class);
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
