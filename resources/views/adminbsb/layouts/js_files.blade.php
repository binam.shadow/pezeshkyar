    <!-- Jquery Core Js -->
    <script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('backend/plugins/node-waves/waves.min.js') }}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('backend/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/morrisjs/morris.js') }}"></script>

    <!-- ChartJs -->
    <script src="{{ asset('backend/plugins/chartjs/Chart.bundle.js') }}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{ asset('backend/plugins/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ asset('backend/plugins/flot-charts/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('backend/plugins/flot-charts/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('backend/plugins/flot-charts/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('backend/plugins/flot-charts/jquery.flot.time.js') }}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('backend/js/admin.js') }}"></script>

    <!-- Demo Js -->
    <script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>

    <script type="text/javascript" src="{{ asset('backend/plugins/block-ui/block-ui.min.js') }}"></script>

    <!-- <script src="{{ asset('backend/js/demo.js') }}"></script> -->
    <script type="text/javascript">
        $('.right-sidebar .demo-choose-skin li').on('click', function () {
            var $body = $('body');
            var $this = $(this);

            var existTheme = $body.data('theme');
            $('.right-sidebar .demo-choose-skin li').removeClass('active');
            $body.removeClass();
            $this.addClass('active');

            $body.attr('data-theme', $this.data('theme'));
            $body.addClass('theme-' + $this.data('theme'));
            $.ajax({
                url: "{{ route('admin.theme_color') }}",
                data: "color=" + $this.data('theme'),
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                },
                error: function (){
                }
            });
        });
    </script>

    <script src="{{ asset('backend/js/custom.js') }}"></script>

	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	</script>
	@yield('scripts')
    @stack('stack_scripts')
