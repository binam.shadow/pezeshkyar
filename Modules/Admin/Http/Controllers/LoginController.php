<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\User;
use Lang;
use Validator;
use Socialite;

class LoginController extends Controller {

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('adminbsb.login');
    }

    public function postLogin(Request $request) {
        $validator = Validator::make($request->all(), [
   //         'g-recaptcha-response' => 'required|captcha'
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $credentials = [
            'email'     => $request->email,
            'password'  => $request->password,
            'active'    => 1
        ];
        $errors = new Collection();
        $errors->push(Lang::get('admin::default.login_incorrect'));
        if (Auth::attempt($credentials)){
            $user = User::where('email', $request->email)->first();
            Auth::login($user);
            return redirect()->route('admin.index');
        } else
            return view('adminbsb.login')->with('errors', $errors);
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function redirectToGoogle(Request $request) {
        return Socialite::driver('google')
            ->scopes(['openid', 'profile', 'email'])
            ->redirect();
    }

    public function handleGoogleCallback(Request $request) {
        $data = Socialite::driver('google')->user();

        $email = $data->email;
        $emailAliasses = [
            explode("@", $email)[0] . '@gmail.com',
            explode("@", $email)[0] . '@googlemail.com'
        ];

        $user = User::whereIn('email', $emailAliasses)->first();

        if ($user) {
            Auth::login($user);
            return redirect()->route('admin.index');
        }

        $errors = new Collection();
        $errors->push(Lang::get('admin::default.login_incorrect'));
        return redirect()->route('admin.login')->with('errors', $errors);
    }
}
