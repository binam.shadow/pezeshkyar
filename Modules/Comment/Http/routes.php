<?php

Route::group(['middleware' => 'web', 'prefix' => 'comments', 'namespace' => 'Modules\Comment\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'comment.list',
        'uses'	=> 'CommentController@index'
    ]);
    Route::get('create', [
        'as'	=> 'comment.create',
        'uses'	=> 'CommentController@create'
    ]);
    Route::post('store', [
        'as'	=> 'comment.store',
        'uses'	=> 'CommentController@store'
    ]);
    Route::get('{comment}/edit', [
        'as'	=> 'comment.edit',
        'uses'	=> 'CommentController@edit'
    ]);
    Route::post('{comment}', [
        'as'	=> 'comment.update',
        'uses'	=> 'CommentController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'comment.dataList',
        'uses'	=> 'CommentController@commentList'
    ]);
    Route::delete('{comment}', [
        'as'	=> 'comment.delete',
        'uses'	=> 'CommentController@destroy'
    ]);
});