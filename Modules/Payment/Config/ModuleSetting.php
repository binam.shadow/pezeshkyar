<?php
namespace Modules\Payment\Config;
use App\Menu;
use App\Permission;
use App\Role;
use App\User;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'Payment';
    public $fa_name = 'پرداخت';

    /**
     *
     */
    public function setup() {
		$role = Role::where('name', 'super-admin')->first();

        // Edit Payment Permission
        $permissions = [
            'display_name' => 'تنظیمات درگاه '
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.edit'], $permissions);

		$role->attachPermission($edit);

        // Add Payment Menu to Setting sub menu
        $menu_parent = Menu::where('name', 'Setting')->first();
        $menu = [
            'display_name' => 'تنظیمات درگاه ' ,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name) . '.edit'
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name) . '.edit',
        ], $menu);

        Artisan::call('module:migrate', ['module' => $this->en_name]);

    }

    /**
     * @throws \Exception
     */
    public function remove() {

        // Remove edit payment permission
        Permission::where('name', 'like', strtolower($this->en_name).'%')->delete();

        // Remove edit payment setting Menu
        Menu::where('name', strtolower($this->en_name) . '.edit')->delete();

        Artisan::call('module:migrate-rollback', ['module' => $this->en_name ]);
    }
}
