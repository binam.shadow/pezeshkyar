<?php

Route::group(['middleware' => 'web', 'prefix' => 'notifications', 'namespace' => 'Modules\Notification\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'notification.list',
        'uses'	=> 'NotificationController@index'
    ]);
    Route::get('create', [
        'as'	=> 'notification.create',
        'uses'	=> 'NotificationController@create'
    ]);
    Route::post('store', [
        'as'	=> 'notification.store',
        'uses'	=> 'NotificationController@store'
    ]);
    Route::get('{notification}/edit', [
        'as'	=> 'notification.edit',
        'uses'	=> 'NotificationController@edit'
    ]);
    Route::post('{notification}', [
        'as'	=> 'notification.update',
        'uses'	=> 'NotificationController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'notification.dataList',
        'uses'	=> 'NotificationController@notificationList'
    ]);
    Route::delete('{notification}', [
        'as'	=> 'notification.delete',
        'uses'	=> 'NotificationController@destroy'
    ]);
});