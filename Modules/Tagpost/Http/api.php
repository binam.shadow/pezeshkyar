<?php
Route::group([
 //   'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Tagpost\Http\Controllers'
], function() {

    Route::get('gettagpost/{tagid}', [
        'uses'	=> 'ApiTagpostController@index'
    ]);
    Route::post('addtagpost', [
        'uses'	=> 'ApiTagpostController@store'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/