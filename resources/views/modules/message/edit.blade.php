@extends('adminbsb.layouts.master')

@section('title')
@lang('message::default.create')
@endsection

@section('styles')
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="{{ asset('backend/plugins/auto-complete/easy-autocomplete.min.css') }}" rel="stylesheet" />
<link href="{{ asset('backend/plugins/auto-complete/easy-autocomplete.themes.min.css') }}" rel="stylesheet" />
<style>
	.easy-autocomplete-container ul{
		padding-right: 0;
	}
	.easy-autocomplete {
		width: 212px !important;
	}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('message::default.edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form id="messageEdit" method="post" action="{{ route('message.edit', ['id' => $message->id]) }}">
					{{ csrf_field() }}
					<div class="form-group form-float">
                        <div class="form-line">
                            <input class="form-control" name="title" value="{{ $message->title }}" required="" aria-required="true" type="text">
                        	<label class="form-label">@lang('message::default.title') <span class="col-red">*</span></label>
                        </div>
                    </div>
					<div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ $message->description }}</textarea>
                        	<label class="form-label">@lang('message::default.description') <span class="col-red">*</span></label>
                        </div>
                    </div>
					<div class="form-group form-float">
						<label class="form-label">@lang('message::default.type') <span class="col-red">*</span></label>
                        <div>
							<select class="col-md-8" name="type">
								<option value="sms" @if($message->type == 'sms') {{'selected'}} @endif>پیامک (sms)</option>
								<option value="notification" @if($message->type == 'notification') {{'selected'}} @endif>پیام در اپ (notification)</option>
							</select>
                        </div>
                    </div>
					<input type="hidden" name="receiver_id" value="{{$message->receiver_id}}">
					<div class="clearfix">
						<div class="demo-radio-button">
							<input name="group5" id="toAll" class="with-gap radio-col-red" @if($message->receiver_id == 0) checked @endif type="radio">
							<label for="toAll">ارسال به همه</label>
						</div>
					</div>
					<div class="form-group form-float">
						<input name="group5" id="toOne" class="with-gap radio-col-red" @if($message->receiver_id != 0) checked @endif type="radio">
						<label for="toOne">ارسال به یک نفر</label>
						<input id="user_id" name="users" @if($message->receiver_id != 0) value="{{$user->phone}}" @else class="hidden" @endif placeholder="قسمتی از شماره" aria-required="true" type="text">
					</div>
					<div>در صندوق پیام نمایش داده شود؟</div>
                    <div class="switch">
                        <label><input name="show_in_inbox" @if($message->show_in_inbox) checked @endif type="checkbox"><span class="lever switch-col-cyan"></span></label>
                    </div>
                    <div class="form-group form-float">
                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('message::default.edit')</button>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/config.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/lang/fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/auto-complete/jquery.easy-autocomplete.min.js') }}"></script>

<script type="text/javascript">
$("#toAll").click(function () {
	$("#user_id").addClass("hidden");
	$("input[name=receiver_id]").val(0);
});
$("#toOne").click(function () {
	$("#user_id").removeClass("hidden");
	$("#user_id").val('').trigger('change');
});
var options = {
	url: function(phrase) {
		return "{{route('message.getUsers')}}";
	},
	getValue: function(element) {
		return element.phone;
	},
	ajaxSettings: {
		dataType: "json",
		method: "GET",
		data: {
	  		dataType: "json"
		}
	},
	list: {
        onSelectItemEvent: function() {
            var selectedItemValue = $("#user_id").getSelectedItemData().id;
            $("input[name=receiver_id]").val(selectedItemValue);
        }
    },
	preparePostData: function(data) {
		data.phone = $("#user_id").val();
		return data;
	},
};

$("#user_id").easyAutocomplete(options);
</script
@endsection
