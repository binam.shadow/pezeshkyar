<div class="row">
    <div class="col-lg-6">
        <div class="form-group form-float">
            <label class="form-label">@lang('notification::default.notificationname') <span
                        class="col-red">*</span></label>
            <div class="form-line">
                <input class="form-control" name="name" value="{{ old('name', $notification->name) }}"
                       aria-required="true" type="text">
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group form-float">
            <label class="form-label">@lang('notification::default.priority') <span
                        class="col-red">*</span></label>
            <div class="">
                <select id="select-priority"
                        class="col-md-8" name="priority" value="{{ old('priority', $notification->priority) }}">
                    @for($i=1;$i<9;$i++)
                        <option value="{{ $i }}" @if($i == $notification->priority) selected @endif>{{ $i }}</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-offset-4 col-md-4 text-center">
        <p class="text-center">انتخاب آیکن</p>
        <div class="col-sm-5 col-center text-center">
            <img style="width: 100%" id="icon-show" @if($notification->icon == '') src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" @else src="{{ asset('uploads/' . $notification->icon) }}" @endif>
        </div>
        <input id="icon" type="hidden" name="icon" @if($notification->icon != '') value="{{$notification->icon}}" @endif>
        <a href="javascript:void(0)" onclick="deleteImage('icon')">حذف</a> <a class="btn btn-info" type="button" href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/categories&relative_url=1&type=1&amp;popup=1&amp;field_id=icon')}}')" >انتخاب تصویر</a>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <button type="submit"
                class="btn btn-primary btn-lg m-t-15 waves-effect">
            @lang('notification::default.'.$saveButtonLabel)</button>
    </div>
</div>