<?php

Route::group(['middleware' => 'web', 'prefix' => 'tabposts', 'namespace' => 'Modules\Tabpost\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'tabpost.list',
        'uses'	=> 'TabpostController@index'
    ]);
    Route::get('create', [
        'as'	=> 'tabpost.create',
        'uses'	=> 'TabpostController@create'
    ]);
    Route::post('store', [
        'as'	=> 'tabpost.store',
        'uses'	=> 'TabpostController@store'
    ]);
    Route::get('{tabpost}/edit', [
        'as'	=> 'tabpost.edit',
        'uses'	=> 'TabpostController@edit'
    ]);
    Route::patch('{tabpost}', [
        'as'	=> 'tabpost.update',
        'uses'	=> 'TabpostController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'tabpost.dataList',
        'uses'	=> 'TabpostController@tabpostList'
    ]);
    Route::delete('{tabpost}', [
        'as'	=> 'tabpost.delete',
        'uses'	=> 'TabpostController@destroy'
    ]);
});