<?php
namespace Modules\Paylist\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use DataTables;
 use Modules\Paylist\Entities\Paylist;


class PaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('paylist::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $paylist = new Paylist();
        return view('paylist::create', compact('paylist'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('paylist.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Paylist::create($request->all());
        return back()
            ->with('msg', \Lang::get('paylist::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Paylist $paylist)
    {
        return view('paylist::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Paylist $paylist)
    {
        return view('paylist::edit', compact('paylist'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Paylist $paylist, Request $request)
    {
        Entrust::can('paylist.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $paylist->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Paylist $paylist)
    {
        try {
            $paylist->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('paylist.list')
                ->with('msg', \Lang::get('paylist::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function paylistList(){
          return DataTables::of(Paylist::query())
                      ->make(true);
     }
}
