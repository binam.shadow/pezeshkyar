<?php

namespace Modules\Payment\Http\Controllers;

use App\City;
use App\Classes\Encryption;
use App\Classes\Smsir;
use App\Province;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Entrust;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Order\Entities\Cart;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Payment\Entities\PaymentToken;
use Modules\Payment\Entities\Transaction;
use Modules\Payment\Http\Classes\Gateway;
use Modules\Product\Entities\Product;
use App\Classes\Notification;

class UserPaymentController extends Controller {
    function getTimeId()
    {
        $genuid = function(){
            return substr(str_pad(str_replace('.','', microtime(true)),12,0),0,12);
        };
        $uid=$genuid();
        while(count(Transaction::find($uid)))
            $uid = $genuid();
        return $uid;
    }

    public function paymentResult(Request $request) {
        if( ! isset($request->State) or $request->State != 'OK')
        {
            Transaction::where('id' , $request->ResNum)->update(['status' => 'FAILED']);
            return "پرداخت ناموفق";
        }
        $transaction = Transaction::where('id' , $request->ResNum)->get()->first();
        if(!count($transaction)){
            return "چنین تراکنشی موجود نیست";
        }
//        else if($transaction->ip != $request->getClientIps()[0]){
//            Transaction::where('id' , $request->ResNum)->update(['status' => 'FAILED']);
//            return "آی پی پرداخت کننده مطابقت ندارد";
//        }
        else if($transaction->status == 'SUCCEED' && $transaction->status == 'FAILED'){
            return "این تراکنش قبلا صورت گرفته است";
        }
        else
        {
            $gateway = \Gateway::verify();
            // update Status in Database Transactions
            Transaction::where('id' , $request->ResNum)->update(['status' => 'SUCCEED']);
            $user = Session::get('user');
            $full_price = 0;
            $destination_nums = [$user->phone];
            $cart = Cart::where('user_id', $user->id)->get();
            $order_data = [
                'tracking_code' => $request->ResNum,
                'user_id' => $user->id,
                'user_name' => $user->name,
                'address' => $user->address,
                'postal_code' => $user->postal_code,
            ];
            if($user->city_id)
                $order_data['city_name'] = City::find($user->city_id)->name;
            if($user->province_id)
                $order_data['province_name'] = Province::find($user->province_id)->name;
            $order = Order::create($order_data);

            if (count($cart)) {
                foreach ($cart as $item) {
                    $order_item = OrderItem::create(['order_id' => $order->id]);
                    $order_item->product_id = $item->product_id;
                    $product = Product::find($item->product_id);
                    if(array_key_exists('is_paid', $product->attributesToArray())){
                        $product->is_paid = 1;
                        $product->save();
                    }
                    $order_item->count = $item->count;
                    $order_item->price = $item->price;
                    $full_price += $item->price * $item->count;
                    $order_item->product_name = Product::find($item->product_id)->name;
                    $order_item->save();
                }
            }
            $order->total_price = $full_price;
            $order->tracking_code = $request->ResNum;
            $order->save();
            $sms_body = ["پرداخت فاکتور با موفقیت انجام شد" . "\n" . "شماره فاکتور" . $request->ResNum];
            Smsir::sendMessage($sms_body, $destination_nums);
            $content = \Lang::get('payment::default.new_payment');
            $notification = new Notification();
            $notification->sendWebNotification($content, url('admin/order/list'));
            Cart::where('user_id', $user->id)->forceDelete();
            Auth::login($user);
            return redirect('/userOrdersList');
        }

    }

    public function paymentStart(Request $request) {
        $user_id = PaymentToken::where('token', $request->token)->first()->user_id;
        if(!$user_id)
            return;
        $user = User::find($user_id);
        Session::put('user', $user);
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $full_price = 0;
        if(count($user)) {
            $cart = Cart::where('user_id', $user->id)->get();
            if (count($cart)) {
                foreach ($cart as $item) {
                    $full_price += $item->price * $item->count;
                }
            }
            try {
                //$tracking_code = $this->getTimeId();
                $gateway = Gateway::saman();
                $gateway->setCallback(url('payment/callbackURL'));
                //$gateway->price(1000)->ready();
                $gateway->price($full_price)->ready();

                return $gateway->redirect();

            } catch (Exception $e) {

                echo $e->getMessage();
            }
        } return "درخواست شما نا معتبر است";
    }
}
