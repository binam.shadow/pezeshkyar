<?php

Route::group(['middleware' => 'web', 'prefix' => 'paylists', 'namespace' => 'Modules\Paylist\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'paylist.list',
        'uses'	=> 'PaylistController@index'
    ]);
    Route::get('create', [
        'as'	=> 'paylist.create',
        'uses'	=> 'PaylistController@create'
    ]);
    Route::post('store', [
        'as'	=> 'paylist.store',
        'uses'	=> 'PaylistController@store'
    ]);
    Route::get('{paylist}/edit', [
        'as'	=> 'paylist.edit',
        'uses'	=> 'PaylistController@edit'
    ]);
    Route::post('{paylist}', [
        'as'	=> 'paylist.update',
        'uses'	=> 'PaylistController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'paylist.dataList',
        'uses'	=> 'PaylistController@paylistList'
    ]);
    Route::delete('{paylist}', [
        'as'	=> 'paylist.delete',
        'uses'	=> 'PaylistController@destroy'
    ]);
});