<?php

namespace Modules\Setting\Http\Controllers;

use App\City;
use App\Classes\Encryption;
use App\Classes\PersianTools;
use App\Classes\Smsir;
use App\Province;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use App\Role;
use Entrust;
use Modules\Setting\Entities\Slider;
use Validator;

class ApiSettingController extends Controller {

    public function getSliders(Request $request) {
        $data = Slider::where(['type' => 'app'])->get();
        return Encryption::encryptor($data, $request->key);
    }

    public function getCities(Request $request) {
        $data = City::where(['province_id' => $request->province_id])->get();
        return Encryption::encryptor($data, $request->key);
    }

    public function getAllCities(Request $request) {
        $data = City::get();
        return Encryption::encryptor($data, $request->key);
    }

    public function getProvinces(Request $request) {
        $data = Province::get();
        return Encryption::encryptor($data, $request->key);
    }

    public function about(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $data = json_decode(Setting::where('key', 'about')->first()->value);
        $data->gallery = json_decode(Setting::where('key', 'about_gallery')->first()->value);
        //$data = json_encode($data); //**comment for Encrypted
        return Encryption::encryptor($data, $request->key);
    }

    public function contact(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        //$data = Setting::where('key', 'contact')->first()->value; //**comment for Encrypted
        $data = json_decode(Setting::where('key', 'contact')->first()->value);// comment for not Encrypted
        return Encryption::encryptor($data, $request->key);
    }

    public function android() {
        $data = array();
        $data['version'] = Setting::where('key', 'android_version')->first()->value;
        $data['active'] = Setting::where('key', 'android_active')->first()->value;
        $data['required'] = Setting::where('key', 'android_required')->first()->value;
        return $data;
    }

    public function ios() {
        $data = array();
        $data['version'] = Setting::where('key', 'ios_version')->first()->value;
        $data['active'] = Setting::where('key', 'ios_active')->first()->value;
        $data['required'] = Setting::where('key', 'ios_required')->first()->value;
        return $data;
    }

    public function openingHours(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $contact = json_decode(Setting::where('key', 'contact')->first()->value);
        $data = ['open' => 0, 'phone' => $contact->phone];
        $opening_hours = json_decode(Setting::where('key', 'opening_hours')->first()->value);
        date_default_timezone_set("Asia/Tehran");
        $weekday = date("l");
        $time = date('H:i');
        $weekday1 = $weekday . 'times1';
        $morning = $opening_hours->$weekday1;
        $morning = explode('-', $morning);
        if($time >= $morning[0] && $time <= $morning[1]){
            $data = ['open' => 1, 'phone' => $contact->phone];
            return Encryption::encryptor($data, $request->key);
        }
        $weekday2 = $weekday . 'times2';
        $evening = $opening_hours->$weekday2;
        $evening = explode('-', $evening);
        if($time >= $evening[0] && $time <= $evening[1]){
            $data = ['open' => 1, 'phone' => $contact->phone];
            return Encryption::encryptor($data, $request->key);
        }
        return Encryption::encryptor($data, $request->key);
    }
}
