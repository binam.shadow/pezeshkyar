@extends('adminbsb.layouts.user')

@section('title')
ورود
@endsection
@section('styles')
<style>
	.rc-anchor-normal .rc-anchor-checkbox-label,
	.rc-anchor-checkbox-label, #recaptcha-anchor-label{
		font-family: irweb !important;
	}
</style>
@endsection

@section('content')
<div class="form-group">
    <a href="{{ route('admin.auth.google') }}" class="btn btn-block bg-red waves-effect" style="background-color: #dd4b39">
        ورود با گوگل
    </a>
</div>
<form action="{{ route('admin.login') }}" id="sign_in" method="POST">
	{{ csrf_field() }}
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<div class="input-group">
		<span class="input-group-addon">
			<i class="material-icons">person</i>
		</span>
		<div class="form-line">
			<input type="text" class="form-control" name="email" placeholder="@lang('admin::default.email')" required >
		</div>
	</div>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="material-icons">lock</i>
		</span>
		<div class="form-line">
			<input type="password" class="form-control" name="password" placeholder="@lang('admin::default.password')" required autocomplete="off">
		</div>
	</div>
	<div class="input-group">
		<div class="col-md-12">
			{!! app('captcha')->display($attributes = [], $options = ['lang'=> 'fa']) !!}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-8 p-t-5">
			<input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
			<label for="rememberme">@lang('admin::default.remember_me')</label>
		</div>
		<div class="col-xs-4">
			<button class="btn btn-block bg-pink waves-effect" type="submit">@lang('admin::default.login')</button>
		</div>
	</div>
</form>
@endsection
