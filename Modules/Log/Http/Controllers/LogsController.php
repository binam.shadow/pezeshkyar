<?php

namespace Modules\Log\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Entrust;
use DataTables;
use Spatie\Activitylog\Models\Activity;
use App\User;
use Morilog\Jalali\Facades\jDateTime;


class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($user = null) {
        Entrust::can('Log.list') ?  : abort(403);
        
        return view('Log::index', compact('user'));
    }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Activity $log) {
        $date_req = explode(' ', $log->created_at);
        $log['persian_created_at'] = $log->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
    
        return $log;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Log $Log)
    {
        return view('Log::edit', compact('Log'));
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Log $Log)
    {
        try {
            $Log->delete();
            // ...
        } catch (\Exception $exception) {
            dd($exception);
            redirect()->route('Log.list')
                ->with('msg', \Lang::get('Log::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
    public function LogList($user = null) {
        Entrust::can('Log.list') ?  : abort(403);

        return DataTables::of(Activity::query())
            ->addColumn('persian_created_at', function ($row) {
                $date_req = explode(' ', $row->created_at);
                return $row->date_req_persian = $date_req[1] . ' ' . jDateTime::strftime('Y-m-d', strtotime($date_req[0]));
            })
            ->make(true);
    }

    public function userData(User $user) {
        Entrust::can('Log.list') ?  : abort(403);

        return $user;
    }
}
