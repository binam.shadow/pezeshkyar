<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class EncriptionMiddleWare
{
    public function handle($request, Closure $next)
    {
        try {
            $key = substr($request->header('Authorization'), 9, 32);
            $request->route()->setParameter('key', $key);
            $config = config('app');
            $encrypted = $request->data;
            $decryptedData = '';
            //(new \Illuminate\Encryption\Encrypter($key, $config['cipher']))->encrypt($decryptedData, false);
            $request_parameters = $request->except('data');
            if(count($request_parameters))
                return response('Forbidden.', 403)->header('Content-Type', 'text/plain');
            if ($request->data) {
                $decryptedData = (new \Illuminate\Encryption\Encrypter($key, $config['cipher']))->decrypt($encrypted, false);
                if (preg_match('/^s:[0-9]*/', $decryptedData) === 1) {
                    $decryptedData = preg_replace('/^s:[0-9]*:"/', '', $decryptedData);
                    $decryptedData = rtrim($decryptedData, '";');
                }
                $data = json_decode($decryptedData, true);
                foreach ($data as $key => $item)
                    $request->route()->setParameter($key, $item);
            }
            return $next($request);
        }catch(\Exception $e){
            return response('Not Found', 404)->header('Content-Type', 'text/plain');
        }
    }
}
