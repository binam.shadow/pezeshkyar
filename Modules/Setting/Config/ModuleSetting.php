<?php
namespace Modules\Setting\Config;
use App\Menu;
use App\Permission;
use App\Role;
use App\Setting;
use App\User;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    /**
     *
     */
    public function setup(){
        $role = Role::where('name', 'super-admin')->first();

        //*******************Setting Create
        $about_edit = Permission::updateOrCreate(['name' => 'about.edit'], ['display_name' => 'ویرایش درباره ما']);
        $opening_hours = Permission::updateOrCreate(['name' => 'contact.opening_hours'], ['display_name' => 'ویرایش ساعات کاری']);
        $contact_edit = Permission::updateOrCreate(['name' => 'contact.edit'], ['display_name' => 'ویرایش تماس با ما']);
        $logo_edit = Permission::updateOrCreate(['name' => 'logo.edit'], ['display_name' => 'ویرایش لوگو']);
        $title_description_edit = Permission::updateOrCreate(['name' => 'title_description.edit'], ['display_name' => 'ویرایش عنوان و توضیحات']);
        $android_version_edit = Permission::updateOrCreate(['name' => 'android_version.edit'], ['display_name' => 'ویرایش ورژن اندروید']);
        $ios_version_edit = Permission::updateOrCreate(['name' => 'ios_version.edit'], ['display_name' => 'ویرایش ورژن ios']);


        $role->attachPermission($about_edit);
        $role->attachPermission($contact_edit);
        $role->attachPermission($opening_hours);
        $role->attachPermission($logo_edit);
        $role->attachPermission($title_description_edit);
        $role->attachPermission($android_version_edit);
        $role->attachPermission($ios_version_edit);

        $contact_types = [
            [
                'name' => 'phone',
                'label'=> 'تلفن'
            ],
            [
                'name' => 'fax',
                'label'=> 'فکس'
            ],
            [
                'name' => 'mobile',
                'label'=> 'موبایل'
            ],
            [
                'name' => 'address',
                'label'=> 'آدرس'
            ],
            [
                'name' => 'email',
                'label'=> 'آدرس'
            ],
            [
                'name' => 'website',
                'label'=> 'وبسایت'
            ],
            [
                'name' => 'telegram',
                'label'=> 'تلگرام'
            ],
            [
                'name' => 'instagram',
                'label'=> 'اینستاگرام'
            ],
            [
                'name' => 'facebook',
                'label'=> 'فیسبوک'
            ],
            [
                'name' => 'twitter',
                'label'=> 'تویتر'
            ],
            [
                'name' => 'piterest',
                'label'=> 'پینترست'
            ],
            [
                'name' => 'linkedin',
                'label'=> 'لینکداین'
            ],
            [
                'name' => 'whatsapp',
                'label'=> 'واتساپ'
            ]
        ];

        Setting::insert([
            ['key'  => 'about', 'value' => '{"name":"","description":"","image":"images/nopic.jpg","active":1}'],
            ['key'  => 'about_gallery', 'value' => '{}'],
            ['key'  => 'opening_hours', 'value' => '{"Saturdaytimes1":"08:00-12:00","Saturdaytimes2":"17:00-21:00","Sundaytimes1":"08:00-12:00","Sundaytimes2":"17:00-21:00","Mondaytimes1":"08:00-12:00",
                "Mondaytimes2":"17:00-21:00","Tuesdaytimes1":"08:00-12:00","Tuesdaytimes2":"17:00-21:00","Wednesdaytimes1":"08:00-12:00","Wednesdaytimes2":"17:00-21:00","Thursdaytimes1":"08:00-12:00","Thursdaytimes2":"","Fridaytimes1":"","Fridaytimes2":""}'],
            ['key'  => 'contact', 'value' => '{"name":"","description":"","phone":"","email":"","contacts": [],"image":"","active":1,"lat":35.68741698177938,"lng":51.38262486457825}'],
            [
                'key'   => 'contact_types',
                'value' => json_encode($contact_types)
            ],
            ['key'  => 'logo', 'value' => 'logo.png'],
            ['key'  => 'title', 'value' => 'رهام وب'],
            ['key'  => 'description', 'value' => 'رهام وب'],
            ['key'  => 'android_version', 'value' => '1'],
            ['key'  => 'android_active', 'value' => '1'],
            ['key'  => 'android_required', 'value' => '1'],
            ['key'  => 'ios_version', 'value' => '1'],
            ['key'  => 'ios_active', 'value' => '1'],
            ['key'  => 'ios_required', 'value' => '1'],
            ['key'  => 'theme_color', 'value' => 'red'],
        ]);

        $setting_en_name = 'Setting';
        Artisan::call('module:migrate', ['module' => $setting_en_name]);
        // Setting main menu
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($setting_en_name)], ['display_name' => 'تنظیمات', 'icon' => 'settings']);

        // Setting sub menu
        $menu = [
            'display_name'  => 'درباره ما',
            'parent_id'     => $menu_parent->id,
            'url'           => 'about.edit'
        ];
        Menu::updateOrCreate(['name' => 'about.edit'], $menu);

        $menu = [
            'display_name'  => 'تماس با ما',
            'parent_id'     => $menu_parent->id,
            'url'           => 'contact.edit'
        ];
        Menu::updateOrCreate(['name' => 'contact.edit'], $menu);

        $menu = [
            'display_name'  => 'ساعت کاری',
            'parent_id'     => $menu_parent->id,
            'url'           => 'contact.opening_hours'
        ];
        Menu::updateOrCreate(['name' => 'contact.opening_hours'], $menu);

        $menu = [
            'display_name'  => 'تغییر لوگو',
            'parent_id'     => $menu_parent->id,
            'url'           => 'logo.edit'
        ];
        Menu::updateOrCreate(['name' => 'logo.edit'], $menu);

        $menu = [
            'display_name'  => 'تغییر عنوان و توضیحات',
            'parent_id'     => $menu_parent->id,
            'url'           => 'title_description.edit'
        ];
        Menu::updateOrCreate(['name' => 'title_description.edit'], $menu);
        $menu = [
            'display_name'  => 'تغییر عنوان و توضیحات',
            'parent_id'     => $menu_parent->id,
            'url'           => 'title_description.edit'
        ];

        $menu = [
            'display_name'  => 'تغییر ورژن اندروید',
            'parent_id'     => $menu_parent->id,
            'url'           => 'android_version.edit'
        ];
        Menu::updateOrCreate(['name' => 'android_version.edit'], $menu);
        $menu = [
            'display_name'  => 'تغییر ورژن ios',
            'parent_id'     => $menu_parent->id,
            'url'           => 'ios_version.edit'
        ];
        Menu::updateOrCreate(['name' => 'ios_version.edit'], $menu);


        /***** Start Province Permission ***/
        $en_name = 'Province';
        $fa_name = 'استان';

        //*******************Province Create, Edit, List
        $permissions = [
            'display_name' => 'ایجاد ' . $fa_name
        ];
        $province_create = Permission::updateOrCreate(['name' => strtolower($en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $fa_name
        ];
        $province_edit = Permission::updateOrCreate(['name' => strtolower($en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $fa_name
        ];
        $province_list = Permission::updateOrCreate(['name' => strtolower($en_name) . '.list'], $permissions);

        $role->attachPermission($province_create);
        $role->attachPermission($province_edit);
        $role->attachPermission($province_list);
        /***** End Province Permission *****/


        /***** Start City Permission *****/
        $en_name = 'City';
        $fa_name = 'شهر';
        //*******************City Create, Edit, List
        $permissions = [
            'display_name' => 'ایجاد ' . $fa_name
        ];
        $city_create = Permission::updateOrCreate(['name' => strtolower($en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $fa_name
        ];
        $city_edit = Permission::updateOrCreate(['name' => strtolower($en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $fa_name
        ];
        $city_list = Permission::updateOrCreate(['name' => strtolower($en_name) . '.list'], $permissions);

        $role->attachPermission($city_create);
        $role->attachPermission($city_edit);
        $role->attachPermission($city_list);
        /***** End City Permission *****/


        /***** Start City And Province Menu *****/
        $setting_en_name = 'Province and City';
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($setting_en_name)], ['display_name' => 'شهر ها و استان ها', 'icon' => 'location_city']);

        // Province menu
        $menu = [
            'display_name'  => 'اضافه کردن استان',
            'parent_id'     => $menu_parent->id,
            'url'           => 'province.create'
        ];
        Menu::updateOrCreate(['name' => 'province.create'], $menu);

        $menu = [
            'display_name'  => 'لیست استان',
            'parent_id'     => $menu_parent->id,
            'url'           => 'province.list'
        ];
        Menu::updateOrCreate(['name' => 'province.list'], $menu);

        // City Menu
        $menu = [
            'display_name'  => 'اضافه کردن شهر',
            'parent_id'     => $menu_parent->id,
            'url'           => 'city.create'
        ];
        Menu::updateOrCreate(['name' => 'city.create'], $menu);

        $menu = [
            'display_name'  => 'لیست شهر',
            'parent_id'     => $menu_parent->id,
            'url'           => 'city.list'
        ];
        Menu::updateOrCreate(['name' => 'city.list'], $menu);
        /***** End City And Province Menu *****/
        /*** Slider ***/
        $slider_en_name = 'Slider';
        $slider_fa_name = 'اسلایدر';
        $permissions = [
            'display_name' => 'ایجاد ' . $slider_fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($slider_en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $slider_fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($slider_en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $slider_fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($slider_en_name) . '.list'], $permissions);

		$role->attachPermission($create);
        $role->attachPermission($edit);
        $role->attachPermission($list);

        $menu = [
            'display_name'  => $slider_fa_name,
            'icon'          => 'important_devices'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($slider_en_name)], $menu);

        // Sub menu
        $menu2 = [
            'display_name' => 'ایجاد ' . $slider_fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($slider_en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($slider_en_name . '.create'),
        ], $menu2);

        $menu2 = [
            'display_name' => 'لیست ' . $slider_fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($slider_en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($slider_en_name . '.list'),
        ], $menu2);
        /*** End ***/
    }

    /**
     * @throws \Exception
     */
    public function remove() {
        Artisan::call('module:migrate-rollback', ['module' => 'Setting']);

        Permission::whereIn('name', ['logo.edit', 'about.edit', 'contact.edit', 'title_description.edit', 'android_version.edit', 'ios_version.edit'])->delete();

        Permission::where('name', 'like', 'province.%')->delete();
        Permission::where('name', 'like', 'city.%')->delete();
        Permission::where('name', 'like', 'slider.%')->delete();

        Menu::where('name', 'like', 'province.%')->delete();
        Menu::where('name', 'like', 'city.%')->delete();
        Menu::where('name', 'like', 'slider.%')->delete();

        Menu::where('name', 'setting')->delete();

        Menu::whereIn('name', ['logo.edit', 'about.edit', 'contact.edit', 'title_description.edit', 'android_version.edit', 'ios_version.edit'])->delete();

        Setting::whereIn('key', ['logo', 'about', 'contact', 'contact_types', 'title', 'description', 'android_version', 'ios_version', 'android_active', 'ios_active', 'android_required', 'ios_required'])->delete();
    }
}
