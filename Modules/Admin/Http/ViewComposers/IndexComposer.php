<?php
namespace Modules\Admin\Http\ViewComposers;
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 7/23/2017
 * Time: 9:04 PM
 */

use App\User;
use Illuminate\View\View;
use Auth;

class IndexComposer
{
    public function __construct()
    {
    }

    /**
     * @param View $view
     */
    public function compose(View $view) {

        $user = Auth::user();
        $new_user_count = User::where('is_seen', 0)->count();

        $view->with([ 'user' => $user, 'new_user_count' => $new_user_count ]);
    }

}
