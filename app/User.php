<?php

namespace App;

use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'avatar', 'phone', 'is_seen', 'one_signal', 'client_type', 'unread_message', 'activation_code',
        'address', 'postal_code', 'province_id', 'city_id', 'birthday', 'sex', 'recommend_code', 'code', 'verified', 'bio', 'logged_in_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function findForPassport($identifier) {
        return $this->where('phone', $identifier)->first();
    }

	public function getThumbAttribute() {
		return $this->media()->exists() ? $this->media()->first() : null;
	}

	public function media() {
		return $this->morphMany(Medium::class, 'mediumable');
	}

    public function comment() {
        return $this->hasMany(\Modules\Comment\Entities\Comment::class);
    }
    public function note() {
        return $this->hasMany(\Modules\Note\Entities\note::class);
    }
    public function favorites() {
        return $this->hasMany(\Modules\Favorite\Entities\favorite::class);
    }
    public function paylist() {
        return $this->hasMany(\Modules\Paylist\Entities\paylist::class);
    }
    public function wallet() {
        return $this->hasMany(\Modules\Wallet\Entities\wallet::class);
    }
}
