@extends('adminbsb.layouts.master')

@section('title')
    @lang('paylist::default.create')
@endsection

@section('styles')
    @include('paylist::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('paylist::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('paylist::partials._errors')
                    <form id="paylistEdit" method="post" action="{{ route('paylist.update', ['paylist' => $paylist]) }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="description" value="{{ old('description') }}" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.description')</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="dargah" value="{{ old('dargah') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.dargah') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="bank_name" value="{{ old('bank_name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="cat_id" value="{{ old('cat_id') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="user_id" value="{{ old('user_id') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('paylist::partials._script')
@endsection
