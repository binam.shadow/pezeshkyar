<?php

namespace Modules\Blog\Http\Controllers;

use App\Classes\Encryption;
use App\Classes\PersianTools;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogCategory;
use App\User;
use App\Role;
use Entrust;
use Modules\Blog\Entities\BlogGallery;
use Validator;

class ApiBlogController extends Controller {

    public function allBlogCategories(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $limit = $request->limit;
        $offset = $request->offset;

        if($limit)
            $data = BlogCategory::limit($limit)->offset($offset)->get();
        else
            $data = BlogCategory::all();
        return Encryption::encryptor($data, $request->key);
    }

    public function blogCategoryChildren(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $limit = $request->limit;
        $offset = $request->offset;
        $id = $request->id;

        if($limit)
            $data = BlogCategory::where('parent_id' , $id)->limit($limit)->offset($offset)->get();
        else
            $data = BlogCategory::where('parent_id' , $id)->get();
        return Encryption::encryptor($data, $request->key);
    }

    public function blogsInCat(Request $request){
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if($request->limit) {
            $limit = $request->limit;
            $offset = $request->offset;
            $data = Blog::where('category_id', $request->id)
                ->orderBy('id', 'desc')
                ->offset($offset)
                ->limit($limit)
                ->get();
        }
        else {
            $data = Blog::where('category_id', $request->id)
                ->orderBy('id', 'desc')
                ->get();
        }
        foreach($data as $item){
            $item->description = strip_tags($item->description);
            $item->gallery = BlogGallery::where('blog_id', $item->id)->get();
        }
        return Encryption::encryptor($data, $request->key);
    }

    public function blogDetails(Request $request){
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if($request->limit) {
            $limit = $request->limit;
            $offset = $request->offset;
            $data = Blog::where('id', $request->id)
                ->offset($offset)
                ->limit($limit)
                ->get();
        }
        else {
            $data = Blog::where('id', $request->id)
                ->get();
        }
        foreach($data as $item){
            $item->description = strip_tags($item->description);
            $item->gallery = BlogGallery::where('blog_id', $request->id)->get();
        }
        return Encryption::encryptor($data, $request->key);
    }

}
