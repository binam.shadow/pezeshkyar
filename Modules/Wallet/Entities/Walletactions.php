<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Model;

class walletactions extends Model
{
    protected $fillable = [
        'wallet_id',
'amount',
'type',
'order_item_id',
'description'
    ];
}
