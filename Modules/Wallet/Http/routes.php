<?php

Route::group(['middleware' => 'web', 'prefix' => 'wallets', 'namespace' => 'Modules\Wallet\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'wallet.list',
        'uses'	=> 'WalletController@index'
    ]);
    Route::get('create', [
        'as'	=> 'wallet.create',
        'uses'	=> 'WalletController@create'
    ]);
    Route::post('store', [
        'as'	=> 'wallet.store',
        'uses'	=> 'WalletController@store'
    ]);
    Route::get('{wallet}/edit', [
        'as'	=> 'wallet.edit',
        'uses'	=> 'WalletController@edit'
    ]);
    Route::post('{wallet}', [
        'as'	=> 'wallet.update',
        'uses'	=> 'WalletController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'wallet.dataList',
        'uses'	=> 'WalletController@walletList'
    ]);
    Route::delete('{wallet}', [
        'as'	=> 'wallet.delete',
        'uses'	=> 'WalletController@destroy'
    ]);
});