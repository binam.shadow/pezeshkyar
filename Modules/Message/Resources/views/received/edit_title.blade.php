@extends('adminbsb.layouts.master')

@section('title')
@lang('message::default.received_message_titles')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
<style>
    .btn-del {
        position: absolute;
        left: 0;
        top: 0px;
    }
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6">
                        <h2>@lang('message::default.received_message_titles')</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <form action="{{ route('message.received_title_edit') }}" method="post">
                    {{ csrf_field() }}
                    <div id="inputbox">
                        @foreach($titles as $title)
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="titles[]" value="{{ $title->value }}" placeholder="@lang('message::default.title')" required aria-required="true" type="text">
                                <i class="btn btn-sm btn-danger btn-del material-icons">close</i>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group form-float">
                        <button type="button" onclick="addbox()" class="btn bg-green waves-effect">
                            <i class="material-icons">add</i>
                            <span>@lang('message::default.add_title')</span>
                        </button>
                    </div>
                    <div class="form-group form-float">
                        <button type="submit" class="btn btn-primary btn-lg waves-effect">@lang('message::default.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
@include('adminbsb.layouts.datatable_files')
<script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
    function addbox() {
        var txt = '<div class="form-group form-float"><div class="form-line">'+
            '<input class="form-control" name="titles[]" placeholder="@lang('message::default.title') *" aria-required="true" required type="text"><i class="btn btn-sm btn-danger btn-del material-icons">close</i>'+
        '</div></div>';
        $('#inputbox').append(txt);
    }
    $('body').on('click', '.btn-del', function(){
        $(this).parents('.form-group').remove();
    });
    function showConfirmDelete(id){
        swal({
            title: "@lang('message::default.are_you_sure')",
            text: "@lang('message::default.warning_delete_msg')",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "@lang('message::default.cancel')",
            confirmButtonText: "@lang('message::default.confirm')",
            closeOnConfirm: false
        }, function (action) {
            if(action)
                $.ajax({
                    url: "{{ route('message.received_delete') }}",
                    data: "id="+id,
                    processData: false,
                    contentType: false,
                    type: 'GET',
                    success: function (result) {
                        if(result.status == 'success') {
                            swal("@lang('message::default.success_delete')", "@lang('message::default.success_delete_msg')", "success");
                            dataTable.ajax.reload()
                        }
                    },
                    error: function (){

                    }
                });
        });
    }
</script>
@endsection