<?php

namespace Modules\Notification\Entities;

use Illuminate\Database\Eloquent\Model;

class notification extends Model
{
    protected $fillable = ['name',
        'description'];
}
