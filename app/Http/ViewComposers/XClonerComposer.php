<?php
namespace App\Http\ViewComposers;
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 7/23/2017
 * Time: 9:04 PM
 */

use Illuminate\View\View;
use App\Setting;
// citiesinfo

class XClonerComposer
{
    public function __construct()
    {
    }

    /**
     * @param View $view
     */
    public function compose(View $view) {
        $contact_types = json_decode(Setting::where('key', 'contact_types')->first()->value);
    
        $view->with(['contact_types' => $contact_types]);
    }

}
