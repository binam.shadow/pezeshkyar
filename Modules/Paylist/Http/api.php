<?php
Route::group([
 //   'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Paylist\Http\Controllers'
], function() {

    Route::post('addpaylist', [
        'uses'	=> 'ApiPaylistController@store'
    ]);
    Route::get('userpaylist', [
        'uses'	=> 'ApiPaylistController@user'
    ]);
    Route::get('allpaylist', [
        'uses'	=> 'ApiPaylistController@all'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/