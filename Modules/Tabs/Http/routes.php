<?php

Route::group(['middleware' => 'web', 'prefix' => 'tabss', 'namespace' => 'Modules\Tabs\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'tabs.list',
        'uses'	=> 'TabsController@index'
    ]);
    Route::get('create', [
        'as'	=> 'tabs.create',
        'uses'	=> 'TabsController@create'
    ]);
    Route::post('store', [
        'as'	=> 'tabs.store',
        'uses'	=> 'TabsController@store'
    ]);
    Route::get('{tabs}/edit', [
        'as'	=> 'tabs.edit',
        'uses'	=> 'TabsController@edit'
    ]);
    Route::patch('{tabs}', [
        'as'	=> 'tabs.update',
        'uses'	=> 'TabsController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'tabs.dataList',
        'uses'	=> 'TabsController@tabsList'
    ]);
    Route::delete('{tabs}', [
        'as'	=> 'tabs.delete',
        'uses'	=> 'TabsController@destroy'
    ]);
});