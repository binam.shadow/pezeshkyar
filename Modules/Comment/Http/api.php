<?php
Route::group([
 //   'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Comment\Http\Controllers'
], function() {

    Route::get('comment/{postid}', [
        'uses'	=> 'ApiCommentController@commentslist'
    ]);
    Route::post('addcomment', [
        'uses'	=> 'ApiCommentController@store'
    ]);
    Route::get('comment/{Comment}', [
        'uses'	=> 'ApiCommentController@single'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/