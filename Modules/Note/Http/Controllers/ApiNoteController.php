<?php
namespace Modules\Note\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Note\Entities\Note;


class ApiNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($postid ,$request)
    {
        $cat = Note::where([
            'post_id' => 33,
         //   'user_id' => 1
        ])->get();
        return $cat;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $note = new Note();
        return view('note::create', compact('note'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
 //       Entrust::can('note.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Note::create($request->all());
        return back()
            ->with('msg', \Lang::get('note::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Note $note)
    {
        return view('note::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Note $note)
    {
        return view('note::edit', compact('note'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Note $note, Request $request)
    {
        Entrust::can('note.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $note->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Note $note)
    {
        try {
            $note->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('note.list')
                ->with('msg', \Lang::get('note::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function noteList(){
          return DataTables::of(Note::query())
                      ->make(true);
     }
}
