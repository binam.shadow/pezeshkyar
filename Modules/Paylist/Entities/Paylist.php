<?php

namespace Modules\Paylist\Entities;

use Illuminate\Database\Eloquent\Model;

class paylist extends Model
{
    protected $fillable = [
        'name',
        'description',
    'cat_id',
'user_id',
'dargah',
'bank_name'
    ];

    public function cats()
    {
        return $this->belongsTo(\Modules\Product\Entities\ProductCategory::class);
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
