<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Smspanel\Http\Controllers'
], function() {

    Route::get('', [
        'uses'	=> 'ApiSmspanelController@MethodName'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/