<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256)->nullable();
            $table->longText('description')->nullable();
            $table->integer('parent_id')->unsigned()->default(0);
            $table->integer('parent_parent_id')->unsigned()->default(0);
            $table->string('image', 256)->nullable();
			$table->string('icon', 256)->nullable();
            $table->integer('order')->default(0);
            $table->string('price');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
