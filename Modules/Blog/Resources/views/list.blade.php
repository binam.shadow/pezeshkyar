@extends('adminbsb.layouts.master')

@section('title')
@lang('blog::default.blog_list')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('blog::default.blog_list')</h2>
					</div>
				</div>
			</div>
			<div class="body">
                <div class="table-responsive">
    				<table class="table table-bordered table-striped table-hover dataTable js-exportable user-list">
    					<thead>
                            <tr>
                                <th>@lang('blog::default.id')</th>
                                <th>@lang('blog::default.name')</th>
                                <th>@lang('blog::default.description')</th>
                                <th>@lang('blog::default.status')</th>
                                <th>@lang('blog::default.setting')</th>
                            </tr>
                        </thead>
                        <tbody>

                       	</tbody>
    				</table>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
	@include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
	<script type="text/javascript">
		var dataTable = $('.user-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('blog.dataList') }}'
            },
            dom: 'Bfrtip',
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'name', name:'name'},
                {data: 'description'},
                {
                    data: 'active',
                    "bSortable": false,
                    render: function(data, type, row) {
                        if(data == 1){
                            return '<span class="label label-success">@lang('blog::default.active')</span>';
                        } else {
                            return '<span class="label label-danger">@lang('blog::default.deactive')</span>';
                        }
                    }
                }
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function(data, type, row) {
                        var btns = '';
                        @permission('admin.edit')
                            if(row.active == 1)
                                btns += '<button onclick="activate(0, '+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="غیر فعال کردن" type="button" class="btn btn-xs btn-warning waves-effect activate"><i class="material-icons">close</i></button> ';
                            else
                                btns += '<button onclick="activate(1, '+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="فعال کردن" type="button" class="btn btn-xs btn-success waves-effect activate"><i class="material-icons">check</i></button> ';

                            btns += '<a href="{{ route('blog.edit') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                        @endpermission
                        btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
                        return btns;
                    }
                }
            ],
			buttons: [
            	'copyHtml5', 'excelHtml5', 'print'
        	],
        	language:{
        		url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
        	}
		});

        function activate(action, id) {
            showWating();
            $.ajax({
                url: "{{ route('blog.activate') }}",
                data: "action="+action+"&id="+id,
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                    dataTable.ajax.reload()
                },
                error: function (){

                }
            });
        }

        function showConfirmDelete(id){
            swal({
                title: "@lang('blog::default.are_you_sure')",
                text: "@lang('blog::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('blog::default.cancel')",
                confirmButtonText: "@lang('blog::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "{{ route('blog.delete') }}",
                        data: "id="+id,
                        processData: false,
                        contentType: false,
                        type: 'GET',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('blog::default.success_delete')", "@lang('blog::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
	</script>
@endsection
