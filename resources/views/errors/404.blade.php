<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>404</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('backend/favicon.ico') }}" type="image/x-icon">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/plugins/bootstrap/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <!-- Custom Css -->
    <link href="{{ asset('backend/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style-rtl.css') }}" rel="stylesheet">
</head>

<body class="four-zero-four">
    <div class="four-zero-four-container">
        <div class="error-code">404</div>
        <div class="error-message">این صفحه پیدا نشد</div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

</body>

</html>