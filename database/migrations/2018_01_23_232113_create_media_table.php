<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media',function(Blueprint $table){
            $table->increments('id');
            $table->string('caption')->nullable();
            $table->string('file_name');
            $table->string('extension',4);
            $table->string('mime',30);
            $table->integer('size');
            $table->integer('mediumable_id')->nullable();
            $table->string('mediumable_type')->nullable();
            $table->tinyInteger('mediumable_position')->nullable();
            $table->string('manner', 32)->nullable();
            $table->integer('comments_count')->default(0);
            $table->integer('likes_count')->default(0);
            $table->string('description',500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
