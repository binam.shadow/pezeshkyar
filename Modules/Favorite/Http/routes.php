<?php

Route::group(['middleware' => 'web', 'prefix' => 'favorites', 'namespace' => 'Modules\Favorite\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'favorite.list',
        'uses'	=> 'FavoriteController@index'
    ]);
    Route::get('create', [
        'as'	=> 'favorite.create',
        'uses'	=> 'FavoriteController@create'
    ]);
    Route::post('store', [
        'as'	=> 'favorite.store',
        'uses'	=> 'FavoriteController@store'
    ]);
    Route::get('{favorite}/edit', [
        'as'	=> 'favorite.edit',
        'uses'	=> 'FavoriteController@edit'
    ]);
    Route::post('{favorite}', [
        'as'	=> 'favorite.update',
        'uses'	=> 'FavoriteController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'favorite.dataList',
        'uses'	=> 'FavoriteController@favoriteList'
    ]);
    Route::delete('{favorite}', [
        'as'	=> 'favorite.delete',
        'uses'	=> 'FavoriteController@destroy'
    ]);
});