@extends('adminbsb.layouts.master')

@section('title')
    @lang('tabpost::default.create')
@endsection

@section('styles')
    @include('tabpost::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('tabpost::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('tabpost::partials._errors')
                    <form id="tabpostEdit" method="post" action="{{ route('tabpost.update', ['tabpost' => $tabpost])) }}">
                        {{ csrf_field() }}
                        @include('tabpost::_form', ['saveButtonLabel'=> 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('tabpost::partials._script')
@endsection
