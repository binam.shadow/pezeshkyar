<?php
namespace Modules\Admin\Http\ViewComposers;
/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 7/23/2017
 * Time: 9:04 PM
 */

use App\Menu;
use App\Setting;
use Illuminate\View\View;
use Auth;

class MasterComposer
{
    public function __construct()
    {
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $theme_color = Setting::where('key', 'theme_color')->first();
        if(!$theme_color)
            $theme_color = 'red';
        else
            $theme_color = $theme_color->value;

        $view->with(['theme_color' => $theme_color]);
    }

}