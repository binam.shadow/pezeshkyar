<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 256)->nullable();
            $table->string('type', 256)->nullable();
            $table->string('phone', 20)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('receiver_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->boolean('is_seen')->default(0);
			$table->boolean('show_in_inbox')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('messages', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
