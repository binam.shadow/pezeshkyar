<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cart
 * @package Modules\Order\Entities
 * @version September 6, 2017, 8:28 am UTC
 *
 * @property \Modules\Order\Entities\Order product
 * @property \App\User user
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsOrderCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property integer user_id
 * @property integer product_id
 * @property integer count
 * @property bigInteger price
 * @property string features
 */
class Cart extends Model
{
    use SoftDeletes;

    public $table = 'carts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'product_id',
        'count',
        'price',
        'features'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'product_id' => 'integer',
        'count' => 'integer',
        'features' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\Modules\Product\Entities\Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
