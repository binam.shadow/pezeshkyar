<?php

namespace Modules\Tabpost\Entities;

use Illuminate\Database\Eloquent\Model;

class tabpost extends Model
{
    public $fillable = [
        'name',
        'description',
        'link',
        'tab_id',
        // 'image',
        // 'icon'
    ];
    public function media(){
        return $this->morphMany(Tabspost::class, 'mediumable');
    }
}
