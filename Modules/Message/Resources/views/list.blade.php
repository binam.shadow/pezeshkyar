@extends('adminbsb.layouts.master')

@section('title')
@lang('message::default.admin_list')
@endsection

@section('styles')
    <style>
        .swal2-container {
            z-index: 999 !important;
        }
    </style>
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('message::default.admin_list')</h2>
					</div>
				</div>
			</div>
			<div class="body">
                <div class="table-responsive">
    				<table class="table table-bordered table-striped table-hover dataTable js-exportable admin-list">
    					<thead>
                            <tr>
                                <th>@lang('message::default.id')</th>
                                <th>@lang('message::default.title')</th>
                                <th>@lang('message::default.date')</th>
								<th>@lang('message::default.receiver')</th>
                                <th>@lang('message::default.show_in_inbox')</th>
                                <th>@lang('message::default.type')</th>
                                <th>@lang('message::default.setting')</th>
                            </tr>
                        </thead>
                        <tbody>

                       	</tbody>
    				</table>
                </div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="userModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">@lang('order::default.view_description')</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">@lang('order::default.close')</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	@include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert2.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>
	<script type="text/javascript">
		var dataTable = $('.admin-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('message.dataList') }}'
            },
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'title', name:'title'},
                {data: 'date_req_persian', "bSortable": false},
                {
                    data: 'receiver_id',
                    "bSortable": false,
                    render: function(data, type, row) {
                        if(data == 0 || data == null)
                            return '<button data-toggle="tooltip" data-placement="top" title="" data-original-title="همه کاربران" type="button" class="btn btn-xs btn-warning waves-effect activate"><i class="material-icons">check</i></button> ';
                        else
                            return '<button onclick="showData('+data+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="کاربر دریافت کننده" type="button" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">account_circle</i></button> ';
                    }
                },
				{
					data: 'show_in_inbox',
					"bSortable": false,
					render: function(data, type, row) {
                        if(data == 1)
                            return '<span class="label label-success">درحال نمایش</span>';
                        else
                            return '<span class="label label-danger">خیر</span>';
                    }
				},
                {data: 'type'}
            ],
            columnDefs: [
                {
                    targets: 6,
                    render: function(data, type, row) {
                        var btns = '';
                        @permission('message.edit')
                            btns += '<a href="{{ route('message.edit') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                            btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
                        @endpermission
                        btns += '&nbsp;<button onclick="showConfirmSend('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="ارسال به یکی" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">send</i></button>';
                        btns += '&nbsp;<button onclick="showConfirmSendAll('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="ارسال به همه" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">mail</i></button>';
                        return btns;
                    }
                }
            ],
			buttons: [
            	'copyHtml5', 'excelHtml5', 'print'
        	],
        	language:{
        		url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
        	}
		});

        function showConfirmDelete(id){
            swal({
                title: "@lang('message::default.are_you_sure')",
                text: "@lang('message::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('message::default.cancel')",
                confirmButtonText: "@lang('message::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "{{ route('message.delete') }}",
                        data: "id="+id,
                        processData: false,
                        contentType: false,
                        type: 'GET',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('message::default.success_delete')", "@lang('message::default.success_delete_msg')", "success");
                                dataTable.ajax.reload();
                                setTimeout(function(){swal.close();}, 1000);
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
        function showData(id) {
            $.ajax({
                url: "{{ route('message.getUserData') }}",
                data: "id=" + id,
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                    var m_modal = $("#userModal");
                    var text_data = '<p class="col-md-6">نام : '+result.name+'</p>'+
                            '<p class="col-md-6">ایمیل : '+result.email+'</p>'+
                            '<p class="col-md-6">شماره تلفن : '+result.phone+'</p>'+
                            '<p class="col-md-6">کد ملی : '+result.national_id+'</p>'+
                            '<p>آدرس</p>'+result.address;
                    m_modal.find('.modal-body').html(text_data);
                    m_modal.modal("show");
                },
                error: function (){
                    alert('مشکلی به وجود آمده است، لطفا دوباره امتحان کنید');
                }
            });
        }
        function showConfirmSendAll(id){
            swal({
                title: "@lang('message::default.are_you_sure')",
                text: "@lang('message::default.send_all_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('message::default.cancel')",
                confirmButtonText: "@lang('message::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "{{ route('message.send_all') }}",
                        data: "id="+id,
                        processData: false,
                        contentType: false,
                        type: 'GET',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('message::default.success_sent')", "@lang('message::default.success_sent_msg')", "success");
                                dataTable.ajax.reload();
                                setTimeout(function(){swal.close();}, 1000);
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }

        async function showConfirmSend(id) {
            const {value: formValues} = await swal2({
                        title: 'کاربر مورد نظر را انتخاب کنید',
                        html: '<select id="swal-select2" class="select2">' +
                        @foreach($users as $user)
                        '<option value="{{$user->id}}">{{$user->phone}}</option>' +
                        @endforeach
                        '</select>' ,
                        focusConfirm: false,
                        onOpen: function () {
                            $('.select2').select2({
                                width: '100%',
                                dir: 'rtl'
                            });
                        },
                        preConfirm: () => {
                        return $('#swal-select2').val();
                    }
        });
            $.ajax({
                url: "{{route('message.send')}}" + "/" + formValues,
                data: "id="+id,
                processData: false,
                contentType: false,
                type: 'GET',
                success: function (result) {
                    if(result.status == 'success') {
                        swal2("@lang('message::default.success_sent')", "@lang('message::default.success_sent_msg')", "success");
                        dataTable.ajax.reload()
                    }
                },
                error: function (){

                }
            });
        }
    </script>
@endsection
