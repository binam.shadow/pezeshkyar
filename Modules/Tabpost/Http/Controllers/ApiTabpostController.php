<?php
namespace Modules\Tabpost\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Tabpost\Entities\Tabpost;
use Modules\Tabpost\Entities\Tags;

class ApiTabpostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tabpost::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('tabpost.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Tabpost::create($request->all());
        return back()
            ->with('msg', \Lang::get('tabpost::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Tabpost $tabpost)
    {
        return view('tabpost::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Tabpost $tabpost)
    {
        return view('tabpost::edit', compact('tabpost'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Tabpost $tabpost, Request $request)
    {
        Entrust::can('tabpost.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $tabpost->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Tabpost $tabpost)
    {
        try {
            $tabpost->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('tabpost.list')
                ->with('msg', \Lang::get('tabpost::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */

     public function tabpostList(){
          return DataTables::of(Tabpost::query())
                      ->make(true);
     }
    public function tagsdetailslist(){
        $details = Tabpost::where([
            'tab_id' => 1,

        ])->get();

        return $details;

    }
}
