@extends('adminbsb.layouts.master')

@section('title')
@lang('product::default.category_create')
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('product::default.category_create')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="categoryCreate" method="post" action="{{ route('category.create') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
							<label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="description" value="{{ old('description') }}" aria-required="true" type="text">
							<label class="form-label">@lang('product::default.description')</label>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="form-line">
							<input class="form-control" name="price" value="{{ old('price') }}" required="" aria-required="true" type="text">
							<label class="form-label">@lang('product::default.price') <span class="col-red">*</span></label>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب آیکن</p>
							@include('partials._single_fileinput', [
								'field_name' => 'icon'
							])
						</div>
						<div class="col-md-6 text-center">
							<p class="text-center">انتخاب تصویر</p>
							@include('partials._single_fileinput', [
								'field_name' => 'image'
							])
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-6" id="d1">
							<label class="form-label">دسته بندی</label>
							<br>
							<select class="col-md-8 subs" name="subs[]" id="1" onchange="changeCat(event)" >
								<option value="0">مادر</option>
								@foreach($categories as $category)
									<option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
function responsive_filemanager_callback(field_id){
	$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

}
function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
function deleteImage(field_id) {
	$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
	$('#'+field_id).val('');
}
{{--$('select').select2({--}}
	{{--dir: 'rtl'--}}
{{--});--}}
{{--$('select[name=parent_id]').on('change', function () {--}}
	{{--var p_id = $(this).val();--}}
	{{--if(p_id != 0) {--}}
		{{--showWating();--}}
		{{--$.ajax({--}}
			{{--url: "{{ route('category.getChild') }}",--}}
			{{--data: 'parent_id='+p_id+'&parent_parent_id=0',--}}
			{{--type: 'GET',--}}
			{{--success: function (result) {--}}
				{{--$('select[name=parent_parent_id]').html('<option value="0">مادر</option>');--}}
				{{--if(result != '') {--}}
					{{--$.each(result, function (key, value) {--}}
						{{--$('select[name=parent_parent_id]').append('<option value="'+value.id+'">'+value.name+'</option>');--}}
					{{--});--}}
					{{--$('select[name=parent_parent_id]').removeAttr('disabled');--}}
				{{--}--}}
			{{--},--}}
			{{--error: function () {--}}
				{{--alert('مشکلی به وجودآمده است، لطفا دوباره امتحان کنید');--}}
			{{--}--}}
		{{--});--}}
	{{--} else {--}}
		{{--$('.hidden-child').slideUp()--}}
	{{--}--}}
{{--});--}}
{{--function changeCat(event) {--}}
    {{--id = parseInt(event.target.id);--}}
{{--// console.log($('#d'+id));--}}
    {{--$('#d'+id).nextUntil('.form-group').remove();--}}
    {{--var cat_id = $('#'+id).val();--}}
    {{--var plusId = id + 1;--}}

    {{--var category_ids = [];--}}
    {{--$( '.subs').each( function (key, el) {--}}
        {{--if(el.value != undefined && el.value != 0) {--}}
            {{--category_ids.push(parseInt(el.value));--}}
        {{--}--}}

    {{--});--}}
    {{--var category_id = category_ids[category_ids.length - 1];--}}

    {{--if(category_id != undefined) {--}}
{{--// alert("salam")--}}
{{--// showWating();--}}
        {{--$.ajax({--}}
            {{--url: "{{ route('product.getChildren') }}",--}}
            {{--data: 'id='+cat_id+'&category_id='+category_id,--}}
{{--// data: 'id='+cat_id,--}}
            {{--type: 'GET',--}}
            {{--success: function (result) {--}}
                {{--console.log(result);--}}
                {{--if(cat_id != 0) {--}}
                    {{--var selectElement = '<div class="col-md-6"  id="d' + plusId + '">' +--}}
                        {{--'<label class="form-label">زیردسته</label> <br>' +--}}
                        {{--'<select class="col-md-8 subs" name="subs[] " id="' + plusId + '" onchange="changeCat(event)"><option value="0">مادر</option>';--}}
                    {{--if (result.categories.length > 0) {--}}
                        {{--$.each(result.categories, function (key, value) {--}}
                            {{--selectElement += '<option value="' + value.id + '">' + value.name + '</option>';--}}
                        {{--});--}}
                        {{--selectElement += '</div></select>';--}}
                        {{--$('#d' + id).after(selectElement);--}}
                        {{--$('#' + plusId).select2({--}}
                            {{--dir: 'rtl'--}}
                        {{--});--}}
                    {{--}--}}
                {{--}--}}
{{--// $('.attribute-body').remove();--}}
{{--// if(result.attribute_categories.length > 0) {--}}
{{--// $.each(result.attribute_categories, function (c_key, value) {--}}
{{--// var attributeElement =	'<div class="attribute-body"><div class="form-group row"><div class="col-sm-2">';--}}
{{--//             attributeElement += '<label>'+value.name+'</label></div>';--}}
{{--//         $.each(value.attributes, function (key, attribute) {--}}
{{--//         attributeElement += '<div class="col-sm-2">'--}}
{{--//             + '<input id="filter'+attribute.id+'" class="chk-col-cyan with-gap" name="filters['+c_key+'][]" type="radio" value="'+attribute.id+'">'--}}
{{--//             +'<label for="filter'+attribute.id+'">'+attribute.name+' </label></div>';--}}
{{--//         });--}}
{{--//         attributeElement += '</div></div>';--}}
{{--// console.log(attributeElement);--}}
{{--// $('#filters').append(attributeElement);--}}
{{--// });--}}
{{--// }--}}


            {{--},--}}
            {{--error: function () {--}}
                {{--alert('@lang('shopping::default.error_retry')');--}}
            {{--}--}}
        {{--});--}}

    {{--}--}}
{{--// else {--}}
{{--// $('.attribute-body').remove();--}}
{{--// }--}}
{{--}--}}


$("form").submit(function(e){
    var first_select = $( 'select[name=subs]').first();
    console.log(first_select.val());
    console.log(first_select)
    if(first_select.val() == 0) {
        e.preventDefault();
        $('.parent-error').removeClass('display-none')

    }
    else {
        $('.parent-error').addClass('display-none')
    }

});
</script>
@endsection
