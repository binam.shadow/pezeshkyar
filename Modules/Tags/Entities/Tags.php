<?php

namespace Modules\Tags\Entities;

use Illuminate\Database\Eloquent\Model;

class tags extends Model
{
    protected $fillable = ['name'];
}
