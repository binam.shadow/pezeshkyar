@extends('adminbsb.layouts.master')

@section('title')
    @lang('tags::default.create')
@endsection

@section('styles')
    @include('tags::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('tags::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('tags::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('tags.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('tags::partials._script')
@endsection
