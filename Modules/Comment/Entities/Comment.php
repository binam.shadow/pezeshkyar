<?php

namespace Modules\Comment\Entities;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'name',
        'description',
        'user_id',
        'post_id',
        'isactive'
    ];
    public function post()
    {
        return $this->belongsTo(\Modules\Tagpost\Entities\tagpost::class);
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
