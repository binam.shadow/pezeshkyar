@extends('adminbsb.layouts.master')

@section('title')
    @lang('favorite::default.create')
@endsection

@section('styles')
    @include('favorite::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('favorite::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('favorite::partials._errors')
                    <form id="favoriteEdit" method="post" action="{{ route('favorite.update', ['favorite' => $favorite]) }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="cat_id" value="{{ old('cat_id') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="user_id" value="{{ old('user_id') }}" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.description')</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('favorite::partials._script')
@endsection
