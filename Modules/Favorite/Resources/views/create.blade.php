@extends('adminbsb.layouts.master')

@section('title')
    @lang('favorite::default.create')
@endsection

@section('styles')
    @include('favorite::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('favorite::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('favorite::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('favorite.store') }}">
                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('favorite::partials._script')
@endsection
