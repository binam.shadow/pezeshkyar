<?php

Route::group([
    'middleware' => ['web', 'permission:log*'],
    'prefix' => 'admin/log',
    'namespace' => 'Modules\Log\Http\Controllers'
], function() {
    
    Route::get('list/{user?}',[
        'as'	=> 'log.list',
        'uses'	=> 'LogController@index'
    ]);

    Route::get('dataList/{user?}', [
        'as'	=> 'log.dataList',
        'uses'	=> 'LogController@dataList'
    ]);
    
    Route::delete('{log}', [
        'as'	=> 'log.delete',
        'uses'	=> 'LogController@destroy'
    ]);
    
    Route::get('show/{log}', [
        'as'	=> 'log.show',
        'uses'	=> 'LogController@show'
    ]);
    
    Route::get('userData/{user?}', [
        'as'    => 'log.userData',
        'uses'  => 'LogController@userData'
    ]);
});
