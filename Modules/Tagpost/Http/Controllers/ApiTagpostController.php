<?php
namespace Modules\Tagpost\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Tagpost\Entities\Tagpost;


class ApiTagpostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cat = Note::where([
            'tag_id' => 1,
            //   'user_id' => 1
        ])->get();
        return $cat;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tagpost = new Tagpost();
        return view('tagpost::create', compact('tagpost'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
  //      Entrust::can('tagpost.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Tagpost::create($request->all());
        return back()
            ->with('msg', \Lang::get('tagpost::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Tagpost $tagpost)
    {
        return view('tagpost::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Tagpost $tagpost)
    {
        return view('tagpost::edit', compact('tagpost'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Tagpost $tagpost, Request $request)
    {
        Entrust::can('tagpost.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $tagpost->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Tagpost $tagpost)
    {
        try {
            $tagpost->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('tagpost.list')
                ->with('msg', \Lang::get('tagpost::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function tagpostList(){
          return DataTables::of(Tagpost::query())
                      ->make(true);
     }
}
