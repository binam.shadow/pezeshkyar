@extends('adminbsb.layouts.master')

@section('title')
@lang('message::default.show')
@endsection

@section('styles')
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('message::default.show')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<div class="form-group form-float">
					<div class="form-line">
						<input class="form-control" name="name" value="{{ $user->name }}" required="" aria-required="true" type="text">
						<label class="form-label">@lang('message::default.name') <span class="col-red">*</span></label>
					</div>
				</div>
				<div class="form-group form-float">
					<div class="form-line">
						<input class="form-control" name="phone" value="{{ $user->phone }}" required="" aria-required="true" type="text">
						<label class="form-label">@lang('message::default.phone') <span class="col-red">*</span></label>
					</div>
				</div>
				<div class="form-group form-float">
					<div class="form-line">
						<input class="form-control" name="title" value="{{ $message->title }}" required="" aria-required="true" type="text">
						<label class="form-label">@lang('message::default.title') <span class="col-red">*</span></label>
					</div>
				</div>
				<div class="form-group form-float">
					<div class="form-line">
						<textarea class="form-control" rows="10" name="description" required="" aria-required="true" type="text">{{ $message->description }}</textarea>
						<label class="form-label">@lang('message::default.description') <span class="col-red">*</span></label>
					</div>
				</div>
				<div class="form-group form-float">
					<label class="form-label">@lang('message::default.type') <span class="col-red">*</span></label>
					<div class="form-line">
						<input class="form-control" name="type" value="{{ $message->type }}" required="" aria-required="true" type="text">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
@endsection
