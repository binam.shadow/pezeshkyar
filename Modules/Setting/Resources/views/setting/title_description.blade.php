@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.title_description_edit')
@endsection


@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('admin::default.title_description_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<div class="row">
					<form class="col-sm-12" method="post" action="{{ route('title_description.edit') }}" >
						{{ csrf_field() }}
						<div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="title" required="" aria-required="true" value="{{ $data[0] }}">
	                            <label class="form-label">عنوان <span class="col-red">*</span></label>
	                        </div>
	                    </div>
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="description" required="" aria-required="true" value="{{ $data[1] }}">
	                            <label class="form-label">توضیحات <span class="col-red">*</span></label>
	                        </div>
	                    </div>
						<div class="col-xs-6">
							<div class="form-group form-float">
		                        <div class="form-line">
		                            <textarea type="text" class="form-control" name="home_text" required="" aria-required="true">@if(isset($home_text->value)){{ $home_text->value }}@endif</textarea>
		                            <label class="form-label">متن صفحه اصلی <span class="col-red">*</span></label>
		                        </div>
		                    </div>
						</div>
						<div class="col-xs-6">
							<div class="form-group form-float">
		                        <div class="form-line">
		                            <textarea type="text" class="form-control" name="adver_text" required="" aria-required="true">@if(isset($adver_text->value)){{ $adver_text->value }}@endif</textarea>
		                            <label class="form-label">متن صفحه تبلیغات <span class="col-red">*</span></label>
		                        </div>
		                    </div>
						</div>
	                    <div class="form-group form-float">
	                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('admin::default.edit')</button>
	                    </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
