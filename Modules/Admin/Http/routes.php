<?php
Route::get("/filemanager/dialog.php", [
    'middleware'	=> 'web',
    'uses' => 'Modules\Admin\Http\Controllers\AdminController@fileManager'
]);
Route::group([
	'middleware'	=> 'web',
	'prefix'		=> 'rwadmin',
	'namespace'		=> 'Modules\Admin\Http\Controllers'
], function() {

	Route::get('login', [
		'as' => 'admin.login',
		'uses' => 'LoginController@index'
	]);
	Route::post('login', [
		'as' => 'admin.login',
		'uses' => 'LoginController@postLogin'
	]);
	Route::get('auth/google', [
		'as' => 'admin.auth.google',
		'uses' => 'LoginController@redirectToGoogle'
	]);
	Route::get('auth/google/callback', [
		'uses' => 'LoginController@handleGoogleCallback'
	]);
});
Route::group([
	'middleware'	=> 'web',
	'prefix'		=> 'admin',
	'namespace'		=> 'Modules\Admin\Http\Controllers'
], function() {

	Route::get('logout', [
		'as'	=> 'admin.logout',
		'uses'	=> 'LoginController@logout'
	]);

	Route::group(['middleware' => ['web', 'permission:admin*|role*']], function() {

		Route::get('/', [
			'as'	=> 'admin.index',
			'uses'	=> 'AdminHomeController@index'
		]);
		Route::get('theme-color', [
			'as'	=> 'admin.theme_color',
			'uses'	=> 'AdminController@themeColor'
		]);
		Route::get('/activeUsers',[
			'as'	=> 'admin.activeUsers',
			'uses'	=> 'AdminHomeController@activeUsers'
		]);
		/*** Start Admin ***/
		Route::get('create', [
			'as'	=> 'admin.create',
			'uses'	=> 'AdminController@create'
		]);
		Route::post('create', [
			'as'	=> 'admin.create',
			'uses'	=> 'AdminController@store'
		]);
		Route::get('edit/{id?}', [
			'as'	=> 'admin.edit',
			'uses'	=> 'AdminController@edit'
		]);
		Route::post('edit/{id?}', [
			'as'	=> 'admin.edit',
			'uses'	=> 'AdminController@update'
		]);
		Route::get('list', [
			'as'	=> 'admin.list',
			'uses'	=> 'AdminController@list'
		]);
		Route::get('dataList', [
			'as'	=> 'admin.dataList',
			'uses'	=> 'AdminController@dataList'
		]);
		Route::get('activate', [
			'as'	=> 'admin.activate',
			'uses'	=> 'AdminController@activate'
		]);
		Route::get('delete', [
			'as'	=> 'admin.delete',
			'uses'	=> 'AdminController@delete'
		]);
		Route::post('avatarDelete', [
			'as'	=> 'admin.avatarDelete',
			'uses'	=> 'AdminController@avatarDelete'
		]);
		/*** End Admin ***/


		/*** Start role ***/
		Route::get('role-create', [
			'as'	=> 'role.create',
			'uses'	=> 'RoleController@create'
		]);
		Route::post('role-create', [
			'as'	=> 'role.create',
			'uses'	=> 'RoleController@store'
		]);
		Route::get('role-list', [
			'as'	=> 'role.list',
			'uses'	=> 'RoleController@roleList'
		]);
		Route::get('role-data-list', [
			'as'	=> 'role.dataList',
			'uses'	=> 'RoleController@dataList'
		]);
		Route::get('role-edit/{id?}', [
			'as'	=> 'role.edit',
			'uses'	=> 'RoleController@edit'
		]);
		Route::post('role-edit/{id?}', [
			'as'	=> 'role.edit',
			'uses'	=> 'RoleController@update'
		]);
		Route::get('role-delete', [
			'as'	=> 'role.delete',
			'uses'	=> 'RoleController@delete'
		]);
		/*** End role ***/


		// Route::get('/dataTable/{table}', [
		// 	'as'	=> 'admin.dataTable',
		// 	'uses'	=> function ($table){
		// 		return DB::table($table)->get();
		// 	}
		// ]);


	});

});
