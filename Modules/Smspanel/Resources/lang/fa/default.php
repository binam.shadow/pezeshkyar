<?php
/**
 * Created by PhpStorm.
 * User: Saeid
 * Date: 30/12/2017
 * Time: 12:23 AM
 */

return [
    'title' => 'عنوان',
    'list_title' => 'عنوان لیست',
    'name' => 'نام',
    'create' => 'ایجاد',
    'edit' => 'ویرایش',
    'cancel' => 'لغو',
    'confirm' => 'تأیید',
    'success_create' => 'با موفقیت اضافه شد',
    'are_you_sure' => 'آیا به انجام این عملیات اطمینان دارید؟',
    'warning_delete_msg' => 'در صورت حذف، داده‌ها قابل بازیابی نیستند!',
    'success_delete' => 'حذف شد!',
    'success_delete_msg' => 'عملیات حذف با موفقیت انجام شد!',
];
