<?php
Route::group([
 //   'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Catpost\Http\Controllers'
], function() {

    Route::get('getallcatpost', [
        'uses'	=> 'ApiCatpostController@index'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/