<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['type', 'value', 'contactable_type', 'contactable_id'];
    public $timestamps = false;

    public function contacts(){
        return $this->morphTo();
    }
}
