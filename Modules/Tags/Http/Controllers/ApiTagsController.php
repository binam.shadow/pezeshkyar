<?php
namespace Modules\Tags\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Tags\Entities\Tags;


class ApiTagsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tags::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tags = new Tags();
        return view('tags::create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('tags.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Tags::create($request->all());
        return back()
            ->with('msg', \Lang::get('tags::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Tags $tags)
    {
        return view('tags::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Tags $tags)
    {
        return view('tags::edit', compact('tags'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Tags $tags, Request $request)
    {
        Entrust::can('tags.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $tags->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Tags $tags)
    {
        try {
            $tags->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('tags.list')
                ->with('msg', \Lang::get('tags::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function tagsList(){
          return DataTables::of(Tags::query())
                      ->make(true);
     }
    public function getAllTags(Request $request) {
        $limit = $request->limit;
        $offset = $request->offset;

        if($limit)
            $data = Tags::limit($limit)->offset($offset)->get();
        else
            $data = Tags::all();
        //  return Encryption::encryptor($data, $request->key);
        return $data;
    }
}
