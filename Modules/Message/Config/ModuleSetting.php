<?php
namespace Modules\Message\Config;
use App\Menu;
use App\Permission;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'Message';
    public $fa_name = 'پیام';

    /**
     *
     */
    public function setup(){
        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.list'], $permissions);

		$role->attachPermissions($create);
		$role->attachPermissions($edit);
		$role->attachPermissions($list);

        $menu1 = [
            'display_name'  => $this->fa_name,
            'icon'          => 'email'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($this->en_name)], $menu1);

        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.create')
        ];
        Menu::updateOrCreate(['name' => strtolower($this->en_name . '.create'),], $menu2);

        $menu3 = [
            'display_name' => 'آرشیو ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.list'),
        ], $menu3);


        $permissions = [
            'display_name' => 'عناوین ' . $this->fa_name . ' دریافتی'
        ];
        $received_title_edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.received_title_edit'], $permissions);

        $permissions = [
            'display_name' => $this->fa_name . ' دریافتی'
        ];
        $received_edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.received_edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name . ' دریافتی'
        ];
        $received_list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.received_list'], $permissions);

		$role->attachPermissions($received_title_edit);
		$role->attachPermissions($received_edit);
		$role->attachPermissions($received_list);

        $menu2 = [
            'display_name'  => 'عناوین ' . $this->fa_name . ' دریافتی',
            'parent_id'     => $menu_parent->id,
            'name'          => strtolower($this->en_name . '.received_title_edit')
        ];
        Menu::updateOrCreate(['url' => strtolower($this->en_name . '.received_title_edit')], $menu2);

        $menu3 = [
            'display_name' => 'آرشیو ' . $this->fa_name . ' دریافتی',
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.received_list')
        ];
        Menu::updateOrCreate(['name' => strtolower($this->en_name . '.received_list')], $menu3);


        Artisan::call('module:migrate', ['module' => $this->en_name]);

    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', strtolower($this->en_name).'%')->delete();

        Menu::where('name', 'like', strtolower($this->en_name).'%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => $this->en_name ]);
    }
}
