@extends('layouts.user')

@section('title')
ورود
@endsection


@section('content')
<form id="sign_up" method="POST">
	<div class="msg">عضویت</div>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="material-icons">person</i>
		</span>
		<div class="form-line">
			<input type="text" class="form-control" name="namesurname" placeholder="نام و نام خانوادگی" required autofocus>
		</div>
	</div>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="material-icons">email</i>
		</span>
		<div class="form-line">
			<input type="email" class="form-control" name="email" placeholder="ایمیل" required>
		</div>
	</div>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="material-icons">lock</i>
		</span>
		<div class="form-line">
			<input type="password" class="form-control" name="password" minlength="6" placeholder="رمزعبور" required>
		</div>
	</div>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="material-icons">lock</i>
		</span>
		<div class="form-line">
			<input type="password" class="form-control" name="confirm" minlength="6" placeholder="تایید پسورد" required>
		</div>
	</div>
	<div class="form-group">
		<input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
		<label for="terms">من <a href="javascript:void(0);">قوانین</a> را خوانده و قبول دارم</label>
	</div>

	<button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">ثبت نام</button>

	<div class="m-t-25 m-b--5 align-center">
	<a href="sign-in.html">قبلا عضو شده اید؟</a>
	</div>
</form>
@endsection