@extends('adminbsb.layouts.master')

@section('title')
    @lang('favorite::default.list_title')
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>@lang('favorite::default.list_title')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable favorite-list" width="100%">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>@lang('favorite::default.userid')</th>
                                <th>@lang('favorite::default.catid')</th>
                                <th>@lang('favorite::default.setting')</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var dataTable = $('.favorite-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('favorite.dataList') }}'
            },
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'user_id'},
                {data: 'cat_id'},


            ],
            columnDefs: [
                {
                    targets: 3,
                    render: function(data, type, row) {
                        var btns = '';
					@permission('favorite.edit')
                        btns += '<a href="favorites/'+row.id+'/edit'+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                        btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
					@endpermission
                        return btns;
                    }
                }
            ],
            buttons: [
                'copyHtml5', 'excelHtml5', 'print'
            ],
            language:{
                url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        function showConfirmDelete(id){
            swal({
                title: "@lang('favorite::default.are_you_sure')",
                text: "@lang('favorite::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('favorite::default.cancel')",
                confirmButtonText: "@lang('favorite::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "favorites/"+id,
                        //data: "_method=delete",
                        processData: false,
                        contentType: false,
                        type: 'DELETE',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('favorite::default.success_delete')", "@lang('favorite::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
    </script>
@endsection
