 @extends('adminbsb.layouts.master')

@section('title')
@lang('setting::default.opening_hours')
@endsection


@section('content')
        <!-- Widgets -->
<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6">
                        <h2>@lang('setting::default.opening_hours')</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <form class="col-sm-12" id="settingCreate" method="post" action="{{ route('contact.opening_hours') }}">
                        {{ csrf_field() }}

                        <div class="row clearfix">
                            <div class="col-md-6">


                        <div class="form-group form-float">
                            <div class="form-label"><label id="Saturdaytimes1-lbl" for="Saturdaytimes1" class="hasPopover" data-original-title="Saturday">شنبه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Saturdaytimes1" value="<?php echo $opening_hours->Saturdaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Saturdaytimes2" value="<?php echo $opening_hours->Saturdaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-label"><label id="Sundaytimes1-lbl" for="Sundaytimes1" class="hasPopover" data-original-title="Sunday">یکشنبه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Sundaytimes1" value="<?php echo $opening_hours->Sundaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Sundaytimes2" value="<?php echo $opening_hours->Sundaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-label"><label id="Mondaytimes1-lbl" for="Mondaytimes1" class="hasPopover" data-original-title="Monday">دوشنبه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Mondaytimes1" value="<?php echo $opening_hours->Mondaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Mondaytimes2" value="<?php echo $opening_hours->Mondaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-label"><label id="Tuesdaytimes1-lbl" for="Tuesdaytimes1" class="hasPopover" data-original-title="Tuesday">سه شنبه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Tuesdaytimes1" value="<?php echo $opening_hours->Tuesdaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Tuesdaytimes2" value="<?php echo $opening_hours->Tuesdaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-label"><label id="Wednesdaytimes1-lbl" for="Wednesdaytimes1" class="hasPopover" data-original-title="Wednesday">چهار شنبه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Wednesdaytimes1" value="<?php echo $opening_hours->Wednesdaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Wednesdaytimes2" value="<?php echo $opening_hours->Wednesdaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-label"><label id="Thursdaytimes1-lbl" for="Thursdaytimes1" class="hasPopover" data-original-title="Thursday">پنج شنبه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Thursdaytimes1" value="<?php echo $opening_hours->Thursdaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Thursdaytimes2" value="<?php echo $opening_hours->Thursdaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-label"><label id="Fridaytimes1-lbl" for="Fridaytimes1" class="hasPopover" data-original-title="Friday">جمعه</label></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Fridaytimes1" value="<?php echo $opening_hours->Fridaytimes1 ?>" onchange="hourChange()"></div>
                            <div class="form-line"> <input class="form-control" type="text" id="Fridaytimes2" value="<?php echo $opening_hours->Fridaytimes2 ?>" onchange="hourChange()"></div>
                        </div>
                        <input type="hidden" name="openinghours" id="openinghours" value="{{json_encode($opening_hours)}}" />
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('setting::default.opening_hours')</button>
                        </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        function hourChange(){
            var openingHours = document.getElementById('openinghours');
            var Saturdaytimes1 = document.getElementById('Saturdaytimes1').value;
            var Saturdaytimes2 = document.getElementById('Saturdaytimes2').value;
            var Sundaytimes1 = document.getElementById('Sundaytimes1').value;
            var Sundaytimes2 = document.getElementById('Sundaytimes2').value;
            var Mondaytimes1 = document.getElementById('Mondaytimes1').value;
            var Mondaytimes2 = document.getElementById('Mondaytimes2').value;
            var Tuesdaytimes1 = document.getElementById('Tuesdaytimes1').value;
            var Tuesdaytimes2 = document.getElementById('Tuesdaytimes2').value;
            var Wednesdaytimes1 = document.getElementById('Wednesdaytimes1').value;
            var Wednesdaytimes2 = document.getElementById('Wednesdaytimes2').value;
            var Thursdaytimes1 = document.getElementById('Thursdaytimes1').value;
            var Thursdaytimes2 = document.getElementById('Thursdaytimes2').value;
            var Fridaytimes1 = document.getElementById('Fridaytimes1').value;
            var Fridaytimes2 = document.getElementById('Fridaytimes2').value;
            openingHours.value = '{"Saturdaytimes1":"'+Saturdaytimes1+'","Saturdaytimes2":"'+Saturdaytimes2+'","Sundaytimes1":"'+Sundaytimes1+'","Sundaytimes2":"'+Sundaytimes2+'","Mondaytimes1":"'+Mondaytimes1+'","Mondaytimes2":"'+Mondaytimes2+'","Tuesdaytimes1":"'+Tuesdaytimes1+'","Tuesdaytimes2":"'+Tuesdaytimes2+'","Wednesdaytimes1":"'+Wednesdaytimes1+'","Wednesdaytimes2":"'+Wednesdaytimes2+'","Thursdaytimes1":"'+Thursdaytimes1+'","Thursdaytimes2":"'+Thursdaytimes2+'","Fridaytimes1":"'+Fridaytimes1+'","Fridaytimes2":"'+Fridaytimes2+'"}';

        }
    </script>
@endsection