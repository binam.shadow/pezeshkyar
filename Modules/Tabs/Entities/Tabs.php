<?php

namespace Modules\Tabs\Entities;

use Illuminate\Database\Eloquent\Model;

class tabs extends Model
{
    protected $fillable = ['name'];



    public function children() {
        return $this->hasMany(\Modules\Tabpost\Entities\Tabpost::class, 'parent_id');
    }
}
