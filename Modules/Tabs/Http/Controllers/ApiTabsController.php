<?php
namespace Modules\Tabs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
// use Modules\Tabs\Entities\Tabs;


class ApiTabsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tabs::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tabs = new Tabs();
        return view('tabs::create', compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('tabs.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Tabs::create($request->all());
        return back()
            ->with('msg', \Lang::get('tabs::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Tabs $tabs)
    {
        return view('tabs::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Tabs $tabs)
    {
        return view('tabs::edit', compact('tabs'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Tabs $tabs, Request $request)
    {
        Entrust::can('tabs.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $tabs->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Tabs $tabs)
    {
        try {
            $tabs->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('tabs.list')
                ->with('msg', \Lang::get('tabs::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function tabsList(){
          return DataTables::of(Tabs::query())
                      ->make(true);
     }
}
