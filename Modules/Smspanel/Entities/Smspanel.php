<?php

namespace Modules\Smspanel\Entities;

use Illuminate\Database\Eloquent\Model;

class smspanel extends Model
{
    protected $fillable = ['name',
        'description'];
}
