@extends('adminbsb.layouts.master')

@section('title')
    @lang('tagpost::default.create')
@endsection

@section('styles')
    @include('tagpost::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('tagpost::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('tagpost::partials._errors')
                    <form id="tagpostEdit" method="post" action="{{ route('tagpost.update', ['tagpost' => $tagpost]) }}">
                        {{ csrf_field() }}

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('tabpost::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="link" value="{{ old('link') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('tabpost::default.link') <span class="col-red">*</span></label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="image" value="{{ old('image') }}" required="" aria-required="true" type="file">
                                <label class="form-label">@lang('tabpost::default.image') <span class="col-red">*</span></label>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label class="form-label">تگ</label>
                                <select class="col-md-8" name="parent_id">
                                    <option value="0" selected>مادر</option>
                                    @foreach($tags as $tag)
                                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('tagpost::partials._script')
@endsection
