<?php
namespace Modules\User\Config;
use App\Menu;
use App\Permission;
use App\Role;
use Artisan;

/**
 * Created by PhpStorm.
 * User: Mojtaba
 * Date: 8/30/2017
 * Time: 4:45 PM
 */
class ModuleSetting
{
    public $en_name = 'User';
    public $fa_name = 'کاربر';
    /**
     *
     */
    public function setup(){
        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.edit'], $permissions);

        $permissions = [
            'display_name' => 'آرشیو ' . $this->fa_name
        ];
        $list = Permission::updateOrCreate(['name' => strtolower($this->en_name) . '.list'], $permissions);

        $role = ['display_name' => $this->en_name];
        $role = Role::updateOrCreate(['name' => strtolower($this->en_name)], $role);

        // $role->attachPermission($create);
        // $role->attachPermission($edit);
        // $role->attachPermission($list);

        $menu = [
            'display_name'  => $this->fa_name,
            'icon'          => 'account_circle'
        ];
        $menu_parent = Menu::updateOrCreate(['name' => strtolower($this->en_name)], $menu);

        // User sub menu (User > Create User)
        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.create')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.create'),
        ], $menu2);

        // User sub menu (User > Create List)
        $menu2 = [
            'display_name' => 'لیست ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => strtolower($this->en_name . '.list')
        ];
        Menu::updateOrCreate([
            'name' => strtolower($this->en_name . '.list'),
        ], $menu2);

        Artisan::call('module:migrate', ['module' => $this->en_name]);
    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', strtolower($this->en_name) . '%')->delete();

        Role::where(['name' => strtolower($this->en_name)])->delete();

        Menu::where(['name' => strtolower($this->en_name)])->delete();

        Artisan::call('module:migrate-rollback', ['module' => $this->en_name]);
    }
}
