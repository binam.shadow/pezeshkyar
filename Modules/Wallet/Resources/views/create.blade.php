@extends('adminbsb.layouts.master')

@section('title')
    @lang('wallet::default.create')
@endsection

@section('styles')
    @include('wallet::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('wallet::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('wallet::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('wallet.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="amount" value="{{ old('amount') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('wallet::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="bank_name" value="{{ old('bank_name') }}" aria-required="true" type="text">
                                <label class="form-label">@lang('wallet::default.bank_name')</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="shaba" value="{{ old('shaba') }}" aria-required="true" type="text">
                                <label class="form-label">@lang('wallet::default.shaba')</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="clearing_request" value="{{ old('clearing_request') }}" aria-required="true" type="text">
                                <label class="form-label">@lang('wallet::default.clearing')</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('wallet::partials._script')
@endsection
