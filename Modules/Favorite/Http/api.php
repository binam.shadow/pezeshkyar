<?php
Route::group([
 //   'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Favorite\Http\Controllers'
], function() {

    Route::get('getuserfav', [
        'uses'	=> 'ApiFavoriteController@index'
    ]);
    Route::post('addfav', [
        'uses'	=> 'ApiFavoriteController@store'
    ]);

});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/