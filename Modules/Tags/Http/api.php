<?php
Route::group([
//    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Tags\Http\Controllers'
], function() {

    Route::get('getalltags', [
        'uses'	=> 'ApiTagsController@getAllTags'
    ]);

});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/