<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('name', 256)->nullable();
            $table->longText('description')->nullable();
            $table->bigInteger('buy_price')->nullable();
            $table->bigInteger('sell_price')->nullable();
            $table->string('image', 256)->nullable();
            $table->boolean('active')->default(1);
            $table->bigInteger('discount')->nullable();
            $table->integer('discount_percent')->nullable();
            $table->string('url', 256)->nullable();
            $table->string('type', 256)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('products', function($table) {
            $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
