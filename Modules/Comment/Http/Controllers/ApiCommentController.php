<?php
namespace Modules\Comment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Comment\Entities\Comment;

 use Modules\Tagpost\Entities\Tagpost;

class ApiCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('comment::index');
    }

    public function commentslist()
    {
        $cat = Comment::where([
            'post_id' => 1,
            'user_id' => 1
        ])->get();
        return $cat;
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $comment = new Comment();
        return view('comment::create', compact('comment'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
   //     Entrust::can('comment.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);
        $data['user_id'] = $request->postid;
        $data['post_id'] = $request->userid;
        $data['isactive'] = $request->isactive;
        $data = Comment::create($request->all());


        return back()
            ->with('msg', \Lang::get('comment::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Comment $comment)
    {
        return view('comment::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Comment $comment)
    {
        return view('comment::edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Comment $comment, Request $request)
    {
        Entrust::can('comment.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $comment->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Comment $comment)
    {
        try {
            $comment->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('comment.list')
                ->with('msg', \Lang::get('comment::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function commentList(){
          return DataTables::of(Comment::query())
                      ->make(true);
     }
     public function single(Comment $comment){
         $c = new Tagpost();
         $c->comments()->get();

     }
}
