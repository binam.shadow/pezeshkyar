<?php

Route::group([
    'middleware' => ['web', 'permission:message*'],
    'prefix' => 'admin/message',
    'namespace' => 'Modules\Message\Http\Controllers'
], function() {
    Route::get('create', [
        'as'	=> 'message.create',
        'uses'	=> 'MessageController@create'
    ]);
    Route::post('create', [
        'as'	=> 'message.create',
        'uses'	=> 'MessageController@store'
    ]);
    Route::get('edit/{id?}', [
        'as'	=> 'message.edit',
        'uses'	=> 'MessageController@edit'
    ]);
    Route::post('edit/{id?}', [
        'as'	=> 'message.edit',
        'uses'	=> 'MessageController@update'
    ]);
    Route::get('list', [
        'as'	=> 'message.list',
        'uses'	=> 'MessageController@list'
    ]);
    Route::get('dataList', [
        'as'	=> 'message.dataList',
        'uses'	=> 'MessageController@dataList'
    ]);
    Route::get('activate', [
        'as'	=> 'message.activate',
        'uses'	=> 'MessageController@activate'
    ]);
    Route::get('delete', [
        'as'	=> 'message.delete',
        'uses'	=> 'MessageController@delete'
    ]);
    Route::get('send-all', [
        'as'	=> 'message.send_all',
        'uses'	=> 'MessageController@sendAll'
    ]);
    Route::get('getUserData', [
        'as'	=> 'message.getUserData',
        'uses'	=> 'MessageController@getUserData'
    ]);
	Route::get('getUsers', [
        'as'	=> 'message.getUsers',
        'uses'	=> 'MessageController@getUsers'
    ]);
    Route::get('send/{id?}', [
        'as'	=> 'message.send',
        'uses'	=> 'MessageController@send'
    ]);
    // User Messages
    Route::get('receivedTitleEdit', [
        'as'	=> 'message.received_title_edit',
        'uses'	=> 'ReceivedMessageController@getReceivedMessageTitle'
    ]);
    Route::post('receivedTitleEdit', [
        'as'	=> 'message.received_title_edit',
        'uses'	=> 'ReceivedMessageController@postReceivedMessageTitle'
    ]);
    Route::get('receivedList', [
        'as'	=> 'message.received_list',
        'uses'	=> 'ReceivedMessageController@receivedList'
    ]);
    Route::get('receivedDataList', [
        'as'	=> 'message.received_data_list',
        'uses'	=> 'ReceivedMessageController@receivedDataList'
    ]);
    Route::get('receivedDelete', [
        'as'	=> 'message.received_delete',
        'uses'	=> 'ReceivedMessageController@delete'
    ]);
    Route::get('receivedShow/{id?}', [
        'as'	=> 'message.received_show',
        'uses'	=> 'ReceivedMessageController@receivedShow'
    ]);
});
