<?php
namespace $CLASS_NAMESPACE$;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
// use Modules\$NAME$\Entities\$NAME$;


class $CLASS$ extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('$LOWER_NAME$::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $$LOWER_NAME$ = new $NAME$();
        return view('$LOWER_NAME$::create', compact('$LOWER_NAME$'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('$LOWER_NAME$.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        $NAME$::create($request->all());
        return back()
            ->with('msg', \Lang::get('$LOWER_NAME$::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($NAME$ $$LOWER_NAME$)
    {
        return view('$LOWER_NAME$::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($NAME$ $$LOWER_NAME$)
    {
        return view('$LOWER_NAME$::edit', compact('$LOWER_NAME$'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($NAME$ $$LOWER_NAME$, Request $request)
    {
        Entrust::can('$LOWER_NAME$.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $$LOWER_NAME$->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($NAME$ $$LOWER_NAME$)
    {
        try {
            $$LOWER_NAME$->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('$LOWER_NAME$.list')
                ->with('msg', \Lang::get('$LOWER_NAME$::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function $LOWER_NAME$List(){
          return DataTables::of($NAME$::query())
                      ->make(true);
     }
}
