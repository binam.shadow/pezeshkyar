<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TagpostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagposts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id')->unsigned()->default(1);

            $table->string('name', 256)->nullable();
            $table->longText('description')->nullable();
            $table->text('link')->nullable();
            $table->string('image', 256)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('tagposts', function($table) {
            $table->foreign('tag_id')->references('id')->on('tabs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
