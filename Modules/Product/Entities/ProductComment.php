<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductRating
 * @package Modules\Product\Entities
 * @version September 6, 2017, 8:32 am UTC
 *
 * @property \Modules\Product\Entities\Product product
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsProductCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property integer product_id
 * @property string image
 */
class ProductComment extends Model
{
    public $table = 'product_comments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'product_id',
        'user_id',
        'title',
        'description',
        'active',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\Modules\Product\Entities\Product::class);
    }
}
