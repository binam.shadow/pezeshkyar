@extends('adminbsb.layouts.master')

@section('title')
    @lang('catpost::default.create')
@endsection

@section('styles')
    @include('catpost::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('catpost::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('catpost::partials._errors')
                    <form id="catpostEdit" method="post" action="{{ route('catpost.update', ['catpost' => $catpost]) }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6" id="d1">
                                <label class="form-label">دسته بندی</label>
                                <br>
                                <select class="col-md-8 subs" name="subs[]" id="1" onchange="changeCat(event)" >
                                    <option value="0">مادر</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('catpost::partials._script')
@endsection
