<?php
namespace Modules\Smspanel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use DataTables;
 use Modules\Smspanel\Entities\Smspanel;


class SmspanelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('smspanel::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $smspanel = new Smspanel();
        return view('smspanel::create', compact('smspanel'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('smspanel.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Smspanel::create($request->all());
        return back()
            ->with('msg', \Lang::get('smspanel::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Smspanel $smspanel)
    {
        return view('smspanel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Smspanel $smspanel)
    {
        return view('smspanel::edit', compact('smspanel'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Smspanel $smspanel, Request $request)
    {
        Entrust::can('smspanel.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $smspanel->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Smspanel $smspanel)
    {
        try {
            $smspanel->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('smspanel.list')
                ->with('msg', \Lang::get('smspanel::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function smspanelList(){
          return DataTables::of(Smspanel::query())
                      ->make(true);
     }
}
