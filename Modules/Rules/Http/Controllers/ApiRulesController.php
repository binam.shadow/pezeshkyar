<?php
namespace Modules\Rules\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Rules\Entities\Rules;


class ApiRulesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cat = Rules::all();
        return $cat;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $rules = new Rules();
        return view('rules::create', compact('rules'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('rules.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Rules::create($request->all());
        return back()
            ->with('msg', \Lang::get('rules::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Rules $rules)
    {
        return view('rules::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Rules $rules)
    {
        return view('rules::edit', compact('rules'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Rules $rules, Request $request)
    {
        Entrust::can('rules.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $rules->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Rules $rules)
    {
        try {
            $rules->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('rules.list')
                ->with('msg', \Lang::get('rules::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function rulesList(){
          return DataTables::of(Rules::query())
                      ->make(true);
     }
}
