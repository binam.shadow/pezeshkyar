<?php

return [
    'default_disk' => 'local',
//windows
//    'ffmpeg.binaries' => 'D:\ffmpeg\ffmpeg.exe',
//linux
    'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
    'ffmpeg.threads'  => 12,
//windows
//    'ffprobe.binaries' => 'D:\ffmpeg\ffprobe.exe',
//linux
    'ffprobe.binaries' => '/usr/local/bin/ffprobe',

    'timeout' => 3600,
];
