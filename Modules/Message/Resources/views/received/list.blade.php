@extends('adminbsb.layouts.master')

@section('title')
    @lang('message::default.received_list')
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
    @endsection

    @section('content')
            <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>@lang('message::default.received_list')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable received-list">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>@lang('message::default.date')</th>
                                <th>@lang('message::default.title')</th>
                                <th>@lang('message::default.description')</th>
                                <th>@lang('message::default.setting')</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>@lang('message::default.date')</th>
                                <th>@lang('message::default.title')</th>
                                <th>@lang('message::default.description')</th>
                                <th>@lang('message::default.setting')</th>
                            </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    @include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var dataTable = $('.received-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('message.received_data_list') }}'
            },
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'date_req_persian', "bSortable": false},
                {data: 'title', name:'title'},
                {data: 'description', name:'description'}
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function(data, type, row) {
                        var btns = '';
                        @permission('message.received_list')
                                btns += '<a href="{{ route('message.received_show') }}/'+row.id+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="نمایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                        @endpermission
                                btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
                        return btns;
                    }
                }
            ],
            buttons: [
                'copyHtml5', 'excelHtml5', 'print'
            ],
            language:{
                url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
            }
        });


        function showConfirmDelete(id){
            swal({
                title: "@lang('message::default.are_you_sure')",
                text: "@lang('message::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('message::default.cancel')",
                confirmButtonText: "@lang('message::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "{{ route('message.received_delete') }}",
                        data: "id="+id,
                        processData: false,
                        contentType: false,
                        type: 'GET',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('message::default.success_delete')", "@lang('message::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
    </script>
@endsection