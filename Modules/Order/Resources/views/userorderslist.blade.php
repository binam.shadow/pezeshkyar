@extends('order::layouts.master')
@section('title')
@lang('order::default.order_list')
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('order::default.order_list')</h2>
					</div>
				</div>
			</div>
            <div class="body">
                <div class="table-responsive">
    				<table class="table table-bordered table-striped table-hover dataTable js-exportable user-list">
    					<thead>
                            <tr>
                                <th>id</th>
                                <th>@lang('order::default.user_name')</th>
                                <th>@lang('order::default.tracking_code')</th>
                                <th>@lang('order::default.total_price')</th>
                                <th>@lang('order::default.status')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>id</th>
                                <th>@lang('order::default.user_name')</th>
                                <th>@lang('order::default.tracking_code')</th>
                                <th>@lang('order::default.total_price')</th>
                                <th>@lang('order::default.status')</th>
                            </tr>
                        </tfoot>
                        <tbody>

                       	</tbody>
    				</table>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@include('adminbsb.layouts.datatable_files')
	<script type="text/javascript">
		var dataTable = $('.user-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('order.userOrdersDataList') }}'
            },
            dom: 'Bfrtip',
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'user_name', name:'user_name'},
                {data: 'tracking_code'},
                {data: 'total_price'},
                {data: 'status'}
            ],
			buttons: [
        	],
        	language:{
        		url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
        	}
		});
	</script>
@endsection
