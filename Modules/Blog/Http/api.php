<?php
Route::group([
    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Blog\Http\Controllers'
], function() {

    Route::get('allBlogCategories', [
        'uses'	=> 'ApiBlogController@allBlogCategories'
    ]);
    Route::get('blogCategoryChildren', [
        'uses'	=> 'ApiBlogController@blogCategoryChildren'
    ]);
    Route::get('blogsInCat', [
        'uses'	=> 'ApiBlogController@blogsInCat'
    ]);
    Route::get('blogDetails', [
        'uses'	=> 'ApiBlogController@blogDetails'
    ]);
});
