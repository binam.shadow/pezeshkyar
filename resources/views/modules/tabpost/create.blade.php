@extends('adminbsb.layouts.master')

@section('title')
    @lang('tabpost::default.create')
@endsection

@section('styles')
    @include('tabpost::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('tabpost::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('tabpost::partials._errors')
                    <form id="SymbolCreate" method="post" action="{{ route('tabpost.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="name" value="{{ old('name') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.name') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="description" value="{{ old('description') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.description') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="link" value="{{ old('link') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.link') <span class="col-red">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control" name="image" value="{{ old('image') }}" required="" aria-required="true" type="text">
                                <label class="form-label">@lang('product::default.image') <span class="col-red">*</span></label>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-md-6">
                                <label class="form-label">زیردسته</label>
                                <select class="col-md-8" name="parent_id">
                                    <option value="0" selected>مادر</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 parent-parent">
                                <label class="form-label">زیردسته</label>
                                <select class="col-md-8" name="parent_parent_id" disabled>
                                    <option value="0" selected>مادر</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('product::default.category_create')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('tabpost::partials._script')
@endsection
