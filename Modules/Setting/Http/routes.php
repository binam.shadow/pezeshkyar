<?php

Route::group([
    'middleware' => ['web', 'permission:logo*|contact*|about*|title_description*|android_version*|ios_version*|city*|province*|slider*'],
    'prefix' => 'admin/setting',
    'namespace' => 'Modules\Setting\Http\Controllers'
], function() {

    /*** Start Setting ***/
    Route::get('edit-logo', [
        'middleware' => ['web', 'permission:product*'],
        'as'	=> 'logo.edit',
        'uses'	=> 'SettingController@editLogo'
    ]);
    Route::post('edit-logo', [
        'as'	=> 'logo.edit',
        'uses'	=> 'SettingController@updateLogo'
    ]);

    Route::get('opening-hours', [
        'as'	=> 'contact.opening_hours',
        'uses'	=> 'SettingController@editOpeningHours'
    ]);
    Route::post('opening-hours', [
        'as'	=> 'contact.opening_hours',
        'uses'	=> 'SettingController@updateOpeningHours'
    ]);
    Route::get('aboutus', [
        'as'	=> 'about.edit',
        'uses'	=> 'SettingController@editAbout'
    ]);
    Route::post('aboutus', [
        'as'	=> 'about.edit',
        'uses'	=> 'SettingController@updateAbout'
    ]);
    Route::post('about-galleryDelete', [
        'as'	=> 'about.galleryDelete',
        'uses'	=> 'SettingController@aboutGalleryDelete'
    ]);

    Route::get('contactus', [
        'as'	=> 'contact.edit',
        'uses'	=> 'SettingController@editContact'
    ]);
    Route::post('contactus', [
        'as'	=> 'contact.edit',
        'uses'	=> 'SettingController@updateContact'
    ]);

    Route::get('edit-title-description', [
        'as'	=> 'title_description.edit',
        'uses'	=> 'SettingController@getTitleDescriptionEdit'
    ]);
    Route::post('edit-title-description', [
        'as'	=> 'title_description.edit',
        'uses'	=> 'SettingController@postTitleDescriptionEdit'
    ]);

    Route::get('edit-android-version', [
        'as'	=> 'android_version.edit',
        'uses'	=> 'SettingController@editAndroidVersion'
    ]);
    Route::post('edit-android-version', [
        'as'	=> 'android_version.edit',
        'uses'	=> 'SettingController@updateAndroidVersion'
    ]);

    Route::get('edit-ios-version', [
        'as'	=> 'ios_version.edit',
        'uses'	=> 'SettingController@editIosVersion'
    ]);
    Route::post('edit-ios-version', [
        'as'	=> 'ios_version.edit',
        'uses'	=> 'SettingController@updateIosVersion'
    ]);
    /*** End Setting ***/

    /*** Start Province ***/
    Route::get('create-province', [
        'as'    => 'province.create',
        'uses'  => 'ProvinceController@create'
    ]);
    Route::post('create-province', [
        'as'    => 'province.create',
        'uses'  => 'ProvinceController@store'
    ]);

    Route::get('list-province', [
        'as'    => 'province.list',
        'uses'  => 'ProvinceController@list'
    ]);
    Route::get('dataList-province', [
        'as'    => 'province.dataList',
        'uses'  => 'ProvinceController@dataList'
    ]);

    Route::get('edit-province/{id?}', [
        'as'    => 'province.edit',
        'uses'  => 'ProvinceController@edit'
    ]);
    Route::post('edit-province/{id?}', [
        'as'    => 'province.edit',
        'uses'  => 'ProvinceController@update'
    ]);

    Route::get('delete-province', [
        'as'    => 'province.delete',
        'uses'  => 'ProvinceController@delete'
    ]);
    /*** End Province ***/

    Route::get('province-cities', [
        'as'    => 'province.getCities',
        'uses'  => 'CityController@getCities'
    ]);

    /*** Start City ***/
    Route::get('create-city', [
        'as'    => 'city.create',
        'uses'  => 'CityController@create'
    ]);
    Route::post('create-city', [
        'as'    => 'city.create',
        'uses'  => 'CityController@store'
    ]);

    Route::get('list-city', [
        'as'    => 'city.list',
        'uses'  => 'CityController@list'
    ]);
    Route::get('dataList-city', [
        'as'    => 'city.dataList',
        'uses'  => 'CityController@dataList'
    ]);

    Route::get('edit-city/{id?}', [
        'as'    => 'city.edit',
        'uses'  => 'CityController@edit'
    ]);
    Route::post('edit-city/{id?}', [
        'as'    => 'city.edit',
        'uses'  => 'CityController@update'
    ]);

    Route::get('delete-city', [
        'as'    => 'city.delete',
        'uses'  => 'CityController@delete'
    ]);
    /*** End City ***/
    /*** Start Slider ***/
    Route::get('slider-create', [
        'as'    => 'slider.create',
        'uses'  => 'SliderController@create'
    ]);
    Route::post('slider-create', [
        'as'    => 'slider.create',
        'uses'  => 'SliderController@store'
    ]);

    Route::get('slider-list', [
        'as'    => 'slider.list',
        'uses'  => 'SliderController@sliderList'
    ]);
    Route::get('slider-dataList', [
        'as'    => 'slider.dataList',
        'uses'  => 'SliderController@sliderDataList'
    ]);

    Route::get('slider-edit/{id?}', [
        'as'    => 'slider.edit',
        'uses'  => 'SliderController@editSlider'
    ]);
    Route::post('slider-edit/{id?}', [
        'as'    => 'slider.edit',
        'uses'  => 'SliderController@updateSlider'
    ]);
    Route::get('slider-activate', [
        'as'	=> 'slider.activate',
        'uses'	=> 'SliderController@activate'
    ]);
    Route::get('slider-delete', [
        'as'	=> 'slider.delete',
        'uses'	=> 'SliderController@delete'
    ]);
    /*** End Slider ***/
});
