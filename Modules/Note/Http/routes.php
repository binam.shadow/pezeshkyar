<?php

Route::group(['middleware' => 'web', 'prefix' => 'notes', 'namespace' => 'Modules\Note\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'note.list',
        'uses'	=> 'NoteController@index'
    ]);
    Route::get('create', [
        'as'	=> 'note.create',
        'uses'	=> 'NoteController@create'
    ]);
    Route::post('store', [
        'as'	=> 'note.store',
        'uses'	=> 'NoteController@store'
    ]);
    Route::get('{note}/edit', [
        'as'	=> 'note.edit',
        'uses'	=> 'NoteController@edit'
    ]);
    Route::post('{note}', [
        'as'	=> 'note.update',
        'uses'	=> 'NoteController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'note.dataList',
        'uses'	=> 'NoteController@noteList'
    ]);
    Route::delete('{note}', [
        'as'	=> 'note.delete',
        'uses'	=> 'NoteController@destroy'
    ]);
});