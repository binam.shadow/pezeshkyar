@extends('adminbsb.layouts.master')

@section('title')
@lang('setting::default.city_create')
@endsection

@section('styles')
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
.file-footer-caption, .file-thumbnail-footer{
	display: none;
}
</style>
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>تنظیمات درگاه پرداخت</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#mellat" data-toggle="tab" aria-expanded="false"><img src="{{ asset('images/mellat.png') }}" height="90px"></a></li>
                                <li role="presentation"><a href="#saman" data-toggle="tab" aria-expanded="false"><img src="{{ asset('images/sb.png') }}" height="90px"></a></li>
								<li role="presentation"><a href="#bmi" data-toggle="tab" aria-expanded="true"><img src="{{ asset('images/bmi.png') }}" height="90px"></a></li>
                                <li role="presentation"><a href="#parsian" data-toggle="tab" aria-expanded="true"><img src="{{ asset('images/parsian.png') }}" height="90px"></a></li>
                                <li role="presentation"><a href="#zarinpal" data-toggle="tab" style="padding-top: 20px;padding-bottom: 20px;"><img src="{{ asset('images/zarinpal.png') }}" height="70px"></a></li>
                            </ul>
							<div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="mellat">
									<form class="" action="index.html" method="post">
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="username" value="" required="" aria-required="true" type="text">
												<label class="form-label">نام کاربری</label>
											</div>
										</div>
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="password" value="" required="" aria-required="true" type="text" >
												<label class="form-label">رمز عبور</label>
											</div>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="terminalId" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">TerminalId</label>
					                        </div>
					                    </div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="callback_url" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">Callback Url</label>
					                        </div>
					                    </div>
									</form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="saman">
									<form class="" action="index.html" method="post">
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="merchant" value="" required="" aria-required="true" type="text">
												<label class="form-label">مرچنت</label>
											</div>
										</div>
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="password" value="" required="" aria-required="true" type="text" >
												<label class="form-label">رمز عبور</label>
											</div>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="callback_url" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">Callback Url</label>
					                        </div>
					                    </div>
									</form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="bmi">
									<form class="" action="index.html" method="post">
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="merchant" value="" required="" aria-required="true" type="text">
												<label class="form-label">مرچنت</label>
											</div>
										</div>
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="transactionKey" value="" required="" aria-required="true" type="text" >
												<label class="form-label">TransactionKey</label>
											</div>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="terminalId" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">TerminalId</label>
					                        </div>
					                    </div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="callback_url" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">Callback Url</label>
					                        </div>
					                    </div>
									</form>
                                </div>
								<div role="tabpanel" class="tab-pane fade" id="parsian">
									<form class="" action="index.html" method="post">
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="pin" value="" required="" aria-required="true" type="text">
												<label class="form-label">پین</label>
											</div>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="callback_url" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">Callback Url</label>
					                        </div>
					                    </div>
									</form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="zarinpal">
									<form class="" action="index.html" method="post">
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="merchant" value="" required="" aria-required="true" type="text">
												<label class="form-label">مرچنت</label>
											</div>
										</div>
										<div class="form-group form-float">
											<label class="form-label">نوع</label><br>
											<select name="type">
												<option value="zarin-gate">Zarin Gate</option>
												<option value="normal">Normal</option>
											</select>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="callback_url" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">Callback Url</label>
					                        </div>
					                    </div>
										<div class="form-group form-float">
											<label class="form-label">سرور</label><br>
											<select name="server">
												<option value="germany">Germany</option>
												<option value="iran">Iran</option>
												<option value="Test">Test</option>
											</select>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="email" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">ایمیل</label>
					                        </div>
					                    </div>
										<div class="form-group form-float">
											<div class="form-line">
												<input class="form-control" name="mobile" value="" required="" aria-required="true" type="text">
												<label class="form-label">شماره موبایل</label>
											</div>
										</div>
										<div class="form-group form-float">
					                        <div class="form-line">
					                            <input class="form-control" name="description" value="" required="" aria-required="true" type="text">
					                        	<label class="form-label">توضیحات</label>
					                        </div>
					                    </div>
									</form>
                                </div>
                            </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/select2/select2.min.js') }}"></script>
<script>
$('#adminCreate').validate({
	rules: {
		'name': {
			required: true
		}
	}
});
</script>
@endsection
