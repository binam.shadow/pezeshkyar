<?php
Route::group([
  //  'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Wallet\Http\Controllers'
], function() {

    Route::post('charge', [
        'uses'	=> 'ApiWalletController@charge'
    ]);
    Route::get('useracction', [
        'uses'	=> 'ApiWalletController@useraction'
    ]);
    Route::get('allaction', [
        'uses'	=> 'ApiWalletController@index'
    ]);
    Route::post('addaction', [
        'uses'	=> 'ApiWalletController@addaction'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/