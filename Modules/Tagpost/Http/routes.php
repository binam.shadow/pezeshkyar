<?php

Route::group(['middleware' => 'web', 'prefix' => 'tagposts', 'namespace' => 'Modules\Tagpost\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'tagpost.list',
        'uses'	=> 'TagpostController@index'
    ]);
    Route::get('create', [
        'as'	=> 'tagpost.create',
        'uses'	=> 'TagpostController@create'
    ]);
    Route::post('store', [
        'as'	=> 'tagpost.store',
        'uses'	=> 'TagpostController@store'
    ]);
    Route::get('{tagpost}/edit', [
        'as'	=> 'tagpost.edit',
        'uses'	=> 'TagpostController@edit'
    ]);
    Route::post('{tagpost}', [
        'as'	=> 'tagpost.update',
        'uses'	=> 'TagpostController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'tagpost.dataList',
        'uses'	=> 'TagpostController@tagpostList'
    ]);
    Route::delete('{tagpost}', [
        'as'	=> 'tagpost.delete',
        'uses'	=> 'TagpostController@destroy'
    ]);
});