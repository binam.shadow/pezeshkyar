<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaylist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paylists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat_id')->unsigned()->default(1);
            $table->integer('user_id')->unsigned()->default(1);
            $table->string('name', 256)->nullable();
            $table->longText('description')->nullable();
            $table->string('dargah', 30)->nullable();
            $table->string('bank_name', 45)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('paylist', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('cat_id')->references('id')->on('product_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
