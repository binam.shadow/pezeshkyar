<div class="col-md-6 text-center">
	<div class="kv-avatar">
		<!-- <div class="file-loading"> -->
			<input id="avatar" name="{{$field_name}}" type="file">
		<!-- </div> -->
	</div>
	<div class="kv-avatar-hint"><small>Select file < 1500 KB</small></div>
</div>

@push('stack_style')
<link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
<style>
	.file-footer-caption{
		display: none;
	}
	.file-drop-zone, .file-drop-zone.clickable:hover{
		border: none;
	}
	.file-drag-handle {
		display: none;
	}
</style>
@endpush


@push('stack_scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script type="text/javascript">
$("#avatar").fileinput({
	@if(isset($old_image) && !is_null($old_image))
		uploadUrl: "/", // server upload action
	    deleteUrl: "/",
	    showUpload: false, // hide upload button
	    showRemove: true, // hide remove button
		initialPreview: [
			"{{ route('show_img', ['filename' => $old_image->file_name, 'size' => 'lg']) }}"
		],
		initialPreviewFileType: 'image',
		initialPreviewAsData: true,
		initialPreviewShowDelete: true,
		initialPreviewConfig: [
	        {
	            width: '160px',
	            url: "{{route('user.avatarDelete')}}",
	            key: {{$old_image->id}},
	            extra: {}
	        }
	    ],
	@endif
	overwriteInitial: true,
	maxFileSize: 1500,
	showClose: false,
	showCaption: false,
	showUpload: false,
	browseOnZoneClick: true,
	browseLabel: '',
	removeLabel: '',
	browseIcon: '<i class="material-icons">create_new_folder</i>',
	removeIcon: '<i class="material-icons">delete</i>',
	removeTitle: 'Cancel or reset changes',
	showBrowse: false,
	elErrorContainer: '#kv-avatar-errors-1',
	msgErrorClass: 'alert alert-block alert-danger',
	defaultPreviewContent: '<img class="img-responsive" src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}">',
	layoutTemplates: {actions: '<div class="file-actions">' +
			   '    <div class="file-footer-buttons">' +
			   '        {delete} {zoom}' +
			   '    </div>' +
			   '    <div class="clearfix"></div>' +
			   '</div>'},
	allowedFileExtensions: ["jpg", "png", "gif"]
});
</script>
@endpush
