<?php

namespace Modules\Favorite\Entities;

use Illuminate\Database\Eloquent\Model;

class favorite extends Model
{
    protected $fillable = [
        'cat_id',
        'user_id'
    ];

    public function post()
    {
        return $this->belongsTo(\Modules\Product\Entities\ProductCategory::class);
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
