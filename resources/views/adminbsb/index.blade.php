@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.admin_panel')
@endsection


@section('content')
<div class="block-header">
	<h2>@lang('admin::default.admin_panel')</h2>
</div>

<!-- #END# Widgets -->
<!-- CPU Usage -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>CPU USAGE (%)</h2>
					</div>
					<div class="col-xs-12 col-sm-6 align-left">
						<div class="switch panel-switch-btn">
							<span class="m-r-10 font-12">REAL TIME</span>
							<label>OFF<input type="checkbox" id="realtime" checked><span class="lever switch-col-cyan"></span>ON</label>
						</div>
					</div>
				</div>
				<ul class="header-dropdown m-r--5">
					<li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="material-icons">more_vert</i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li><a href="javascript:void(0);">Action</a></li>
							<li><a href="javascript:void(0);">Another action</a></li>
							<li><a href="javascript:void(0);">Something else here</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="body">
				<div id="real_time_chart" class="dashboard-flot-chart"></div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('backend/js/pages/index.js') }}"></script>
<script type="application/javascript">
var data = [], totalPoints = 110;
function getOnlineUsers() {
	y = function () {
		var tmp = null;
		$.ajax({
			'type': "GET",
			'async': false,
			'url': "{{ route('admin.activeUsers') }}",
			'success': function (data) {
				tmp = data*10;
			}
		});
		return tmp;
	}();

	data.push(y);

	var res = [];
	for (var i = 0; i < data.length; ++i) {
		res.push([i, data[i]]);
	}

	return res;
}
</script>
@endsection
