<?php

Route::group(['middleware' => 'web', 'prefix' => 'catposts', 'namespace' => 'Modules\Catpost\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'catpost.list',
        'uses'	=> 'CatpostController@index'
    ]);
    Route::get('create', [
        'as'	=> 'catpost.create',
        'uses'	=> 'CatpostController@create'
    ]);
    Route::post('store', [
        'as'	=> 'catpost.store',
        'uses'	=> 'CatpostController@store'
    ]);
    Route::get('{catpost}/edit', [
        'as'	=> 'catpost.edit',
        'uses'	=> 'CatpostController@edit'
    ]);
    Route::post('{catpost}', [
        'as'	=> 'catpost.update',
        'uses'	=> 'CatpostController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'catpost.dataList',
        'uses'	=> 'CatpostController@catpostList'
    ]);
    Route::delete('{catpost}', [
        'as'	=> 'catpost.delete',
        'uses'	=> 'CatpostController@destroy'
    ]);
});