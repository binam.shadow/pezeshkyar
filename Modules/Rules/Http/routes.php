<?php

Route::group(['middleware' => 'web', 'prefix' => 'ruless', 'namespace' => 'Modules\Rules\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'rules.list',
        'uses'	=> 'RulesController@index'
    ]);
    Route::get('create', [
        'as'	=> 'rules.create',
        'uses'	=> 'RulesController@create'
    ]);
    Route::post('store', [
        'as'	=> 'rules.store',
        'uses'	=> 'RulesController@store'
    ]);
    Route::get('{rules}/edit', [
        'as'	=> 'rules.edit',
        'uses'	=> 'RulesController@edit'
    ]);
    Route::post('{rules}', [
        'as'	=> 'rules.update',
        'uses'	=> 'RulesController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'rules.dataList',
        'uses'	=> 'RulesController@rulesList'
    ]);
    Route::delete('{rules}', [
        'as'	=> 'rules.delete',
        'uses'	=> 'RulesController@destroy'
    ]);
});