@extends('adminbsb.layouts.master')

@section('title')
@lang('admin::default.role_edit')
@endsection


@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('admin::default.role_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form id="roleCreate" method="post" action="{{ route('role.edit', ['id' => $role->id]) }}">
					{{ csrf_field() }}
					<div class="form-group form-float">
                        <div class="form-line">
                            <input class="form-control" name="fa_name" value="{{ $role->display_name }}" required="" aria-required="true" type="text" placeholder="@lang('admin::default.fa_name') (مثال : مدیر) *">
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input class="form-control" name="en_name" value="{{ $role->name }}" required="" aria-required="true" type="text" placeholder="@lang('admin::default.en_name') (مثال : Admin) *">
                        </div>
                    </div>
                    <?php
                    	$role_permissions = json_decode(json_encode($role->permissions->pluck('id')),true);

                    ?>
                    <div class="form-group row">
                        @foreach($permissions as $permission)
                        	@php $i = $permission->id  @endphp
							<div class="col-sm-3">
								<input id="md_checkbox_{{$i}}" @if(in_array($i, $role_permissions)) checked="" @endif class="chk-col-red" name="permissions[]" type="checkbox" value="{{$i}}">
                        		<label for="md_checkbox_{{$i}}">{{ $permission->display_name }}</label>
							</div>
                        @endforeach
                    </div>
                    <div class="form-group form-float">
                        <button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('admin::default.edit')</button>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
	<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
	<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
	<script type="text/javascript">
		$('#roleCreate').validate({
	        roles: {
	            'fa_name': {
	                required: true
	            },
	            'en_name': {
	                required: true,
	                lettersonly: true
	            }
	        },
	        messages: {
	        	'confirm_password':{
	        		equalTo: 'تکرار رمز عبور وارد شده باید با هم برابر باشد',
	        	}
	        }
	    });
	</script>
@endsection