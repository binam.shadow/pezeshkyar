<?php

Route::group(['middleware' => 'web', 'prefix' => 'tagss', 'namespace' => 'Modules\Tags\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'tags.list',
        'uses'	=> 'TagsController@index'
    ]);
    Route::get('create', [
        'as'	=> 'tags.create',
        'uses'	=> 'TagsController@create'
    ]);
    Route::post('store', [
        'as'	=> 'tags.store',
        'uses'	=> 'TagsController@store'
    ]);
    Route::get('{tags}/edit', [
        'as'	=> 'tags.edit',
        'uses'	=> 'TagsController@edit'
    ]);
    Route::post('{tags}', [
        'as'	=> 'tags.update',
        'uses'	=> 'TagsController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'tags.dataList',
        'uses'	=> 'TagsController@tagsList'
    ]);
    Route::delete('{tags}', [
        'as'	=> 'tags.delete',
        'uses'	=> 'TagsController@destroy'
    ]);
});