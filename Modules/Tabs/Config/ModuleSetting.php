<?php
/**
 * Created by PhpStorm.
 * User: Armin
 * Date: 12/03/2018
 * Time: 10:00 PM
 */
namespace Modules\Tabs\Config;

use App\Menu;
use App\Permission;
use Artisan;
use App\Role;

class ModuleSetting
{
    public $en_name = 'Tabs';
    public $fa_name = 'تب';
    public $fa_name_plural = 'تب ها';
    public $icon = 'add_to_photos';

    public function setup(){
        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create_permission = Permission::updateOrCreate(['name' => 'tabs.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit_permission = Permission::updateOrCreate(['name' => 'tabs.edit'], $permissions);

        $permissions = [
            'display_name' => 'لیست ' . $this->fa_name_plural
        ];
        $list_permission = Permission::updateOrCreate(['name' => 'tabs.list'], $permissions);

        $menu1 = [
            'display_name'  => $this->fa_name,
            'icon'          => $this->icon
        ];
        $menu_parent = Menu::updateOrCreate(['name' => 'tabs'], $menu1);

        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => 'tabs.create'
        ];
        Menu::updateOrCreate(['name' => 'tabs.create'], $menu2);

        $menu3 = [
            'display_name' => 'لیست ' . $this->fa_name_plural,
            'parent_id'    => $menu_parent->id,
            'url'          => 'tabs.list'
        ];
        Menu::updateOrCreate([
            'name' => 'tabs.list'
        ], $menu3);

        $role = Role::where('name','super-admin')->first();
        if($role){
            $role->attachPermission($create_permission);
            $role->attachPermission($edit_permission);
            $role->attachPermission($list_permission);
        }

        Artisan::call('module:migrate', ['module' => 'Tabs']);

    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', 'tabs%')->delete();

        Menu::where('name', 'like', 'tabs%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => 'Tabs' ]);
    }
}