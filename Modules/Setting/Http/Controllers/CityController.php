<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Entrust;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Province;
use App\City;

class CityController extends Controller {

    public function getCities(Request $request) {
        return City::where(['province_id' => $request->province_id])->get();
    }

    public function create() {
        Entrust::can('city.create') ? : abort(403);

        $provinces = Province::get();
        return view('setting::city.create', compact('provinces'));
    }

    public function store(Request $request) {
        Entrust::can('city.create') ? : abort(403);

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:256',
            'province'  => 'required',
        ]);
        if($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $exist = City::where([
            'name'  => $request->name,
            'province_id' => $request->province
        ])->first();
        if(count($exist))
            return back()
                ->with('msg', \Lang::get('setting::default.must_unique'))
                ->with('msg_color', 'bg-red');

        City::create([
            'name'  => $request->name,
            'province_id' => $request->province
        ]);

        // Redirect with success message
        return redirect()->route('city.create')
                    ->with('msg', \Lang::get('setting::default.success_create'))
                    ->with('msg_color', 'bg-green');
    }

    public function list()  {
        Entrust::can('city.list') ? : abort(403);

        return view('setting::city.list');
    }

    public function dataList(Request $request) {
        Entrust::can('city.list') ? : abort(403);
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $data['draw'] =  $request->draw;
        $cities = City::with('province')->whereHas('province' , function ($query) use ($request) {
                $query->where('name', 'like', '%'.$request->search['value'].'%');
            })
            ->orderBy($request->columns[$order_column]['data'], $order_dir);
        $result = $cities;
        $count = $result->count();
        if(!$count)
            $result = City::with('province')
                ->where('name', 'like', '%'.$request->search['value'].'%');

        $count = $result->count();
        $data['data'] = $result->limit($request->length)->offset($request->start)->get();

        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();
        return $data;
    }

    public function edit($id = 0) {
        Entrust::can('city.edit') ? : abort(403);

        // Check Province exist
        $city = City::where('id', $id)->first();
        if(!count($city)) abort(404);

        $provinces = Province::get();

        return view('setting::city.edit', compact('city', 'provinces'));
    }

    public function update($id = 0, Request $request) {
        Entrust::can('city.edit') ? : abort(403);

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:256',
            'province'  => 'required',
        ]);
        if($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        // Check Province exist
        $city = City::where('id', $id)->first();
        if(!count($city)) abort(404);

        $city->update([
            'name'  => $request->name,
            'province_id' => $request->province
        ]);

        // Redirect with success message
        return redirect()->route('city.list')
            ->with('msg', \Lang::get('setting::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function delete(Request $request) {
        Entrust::can('city.edit') ? : abort(403);

        City::find($request->id)->forceDelete();
        return ['status' => 'success'];
    }


}
