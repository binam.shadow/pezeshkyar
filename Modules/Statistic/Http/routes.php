<?php

Route::group(['middleware' => 'web', 'prefix' => 'statistic', 'namespace' => 'Modules\Statistic\Http\Controllers'], function()
{
    Route::get('/', 'StatisticController@index');
});
