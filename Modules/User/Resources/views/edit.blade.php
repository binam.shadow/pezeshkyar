@extends('adminbsb.layouts.master')

@section('title')
@lang('user::default.user_edit')
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('user::default.user_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="adminCreate" method="post" action="{{ route('user.edit', ['id' => $user->id]) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row clearfix">
						<div class="col-md-6">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="name" value="{{ $user->name }}" aria-required="true" type="text">
									<label class="form-label">@lang('user::default.name') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="email" value="{{ $user->email }}" aria-required="true" type="text" >
									<label class="form-label">@lang('user::default.email') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="phone" value="{{ $user->phone }}" required="" aria-required="true" type="text">
									<label class="form-label">@lang('user::default.phone') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="password" value="" id="password"  aria-required="true" type="text">
									<label class="form-label">@lang('user::default.password') <span class="col-red">*</span></label>
								</div>
							</div>
							<p>به منظور تغییر ندادن رمز عبور آن را خالی بگذارید</p>
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="confirm_password" value="" aria-required="true" type="text">
									<label class="form-label">@lang('user::default.confirm_password') <span class="col-red">*</span></label>
								</div>
							</div>
						</div>
						@include('partials._avatar_edit', [
							'field_name' => 'avatar',
							'old_image'	=> $user->thumb
						])
					</div>
					<div class="form-group form-float">
						<div class="switch">
							<label>@lang('user::default.active') <input name="active" @if($user->active) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('user::default.deactive')</label>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('user::default.user_edit')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script type="text/javascript">
function responsive_filemanager_callback(field_id){
	$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());

}
function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
function deleteImage(field_id) {
	$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
	$('#'+field_id).val('');
}
// $("#avatar").fileinput({
// 	@if(!is_null($user->thumb))
// 		uploadUrl: "/", // server upload action
// 	    deleteUrl: "/",
// 	    showUpload: false, // hide upload button
// 	    showRemove: true, // hide remove button
// 		initialPreview: [
// 			"{{ route('show_img', ['filename' => $user->thumb->file_name, 'size' => 'lg']) }}"
// 		],
// 		initialPreviewFileType: 'image',
// 		initialPreviewAsData: true,
// 		initialPreviewShowDelete: true,
// 		initialPreviewConfig: [
// 	        {
// 	            width: '160px',
// 	            url: "{{route('user.avatarDelete')}}",
// 	            key: {{$user->thumb->id}},
// 	            extra: {}
// 	        }
// 	    ],
// 	@endif
// 	language: 'fa',
// 	browseOnZoneClick: true,
// 	overwriteInitial: true,
// 	maxFileSize: 1500,
// 	showClose: false,
// 	showCaption: false,
// 	showRemove: false,
// 	showBrowse: false,
// 	browseLabel: '',
// 	removeLabel: '',
// 	browseIcon: '<i class="material-icons">create_new_folder</i>',
// 	removeIcon: '<i class="material-icons">delete</i>',
// 	removeTitle: 'Cancel or reset changes',
// 	elErrorContainer: '#kv-avatar-errors-1',
// 	msgErrorClass: 'alert alert-block alert-danger',
// 	defaultPreviewContent: '<img src="{{ asset('backend/plugins/bootstrap-fileinput/img/default.png') }}" style="width: 140px;margin-bottom: 10px" alt="Your Avatar">',
// 	layoutTemplates: {actions: '<div class="file-actions">\n' +
// 		   '    <div class="file-footer-buttons">\n' +
// 		   '        {delete} {zoom}' +
// 		   '    </div>\n' +
// 		   '    <div class="clearfix"></div>\n' +
// 		   '</div>'},
// 	allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif']
// });


$('#adminCreate').validate({
	rules: {
		'phone': {
			required: true
		},
		'email': {
			email: true
		},
		'password': {
			minlength: 5
		},
		'confirm_password': {
			equalTo: '#password'
		}
	},
	messages: {
		'confirm_password':{
			equalTo: 'تکرار رمز عبور وارد شده باید با هم برابر باشد',
		}
	}
});
</script>
@endsection
