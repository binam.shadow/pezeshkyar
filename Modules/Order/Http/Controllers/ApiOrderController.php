<?php

namespace Modules\Order\Http\Controllers;

use App\City;
use App\Classes\Encryption;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Entrust;
use Illuminate\Support\Facades\DB;
use Modules\Order\Entities\Cart;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderItem;
use Modules\Order\Entities\OrderItemsFeature;
use Modules\Payment\Entities\PaymentToken;
use Modules\Product\Entities\Product;
use App\Classes\Smsir;

class ApiOrderController extends Controller {

    public function createOrderWithoutCart(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if(!$request->product_id){
            $data = ['error' => 1, 'cart_id' => '', 'msg' => \Lang::get('order::default.product_id_needed')];
            return Encryption::encryptor($data, $request->key);
        }

        $data = array();
        $data['user_id'] = $user->id;
        $data['product_id'] = $request->product_id;
        $data['count'] = $request->count;
        $data['price'] = Product::find($request->product_id)->sell_price;

        $cart = Cart::where(['user_id' => $user->id, 'product_id' => $data['product_id']])->get()->first();
        if(count($cart)){
            $cart->count += $data['count'];
            $cart->save();
        }
        else
            Cart::create($data);
        return $this->createOrder($request);
    }

    public function createOrder(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }

        $cart = Cart::where('user_id', $user->id)->get();
        if (!count($cart)){
            $data = ['error' => 2 , 'msg' => 'سبد خرید خالی است'];
            return Encryption::encryptor($data, $request->key);
        }
        $token = str_random(20);
        while(count(PaymentToken::where('token', $token)->first()))
            $token = str_random(20);
        PaymentToken::create(['user_id' => $user->id, 'token' => $token]);
        $data = ['error' => 0, 'token' => $token, 'msg' => 'فاکتور با موفقیت ثبت گردید'];
        return Encryption::encryptor($data, $request->key);
    }

    public function getUserOrders(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $limit = $request->limit;
        $offset = $request->offset;
        $id = $user->id;
        if($limit)
            $data = Order::where('user_id' , $id)
                ->orderBy( 'id' , 'desc' )->limit($limit)->offset($offset)->get();
        else
            $data = Order::where('user_id' , $id)
                ->orderBy( 'id' , 'desc' )->get();

        foreach($data as $item){
            $item->date = jDate::forge($item->created_at)->format('date');
        }
        return Encryption::encryptor($data, $request->key);
    }

    public function getOrderDetails(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $limit = $request->limit;
        $offset = $request->offset;
        $id = $request->order_id;
        if(!count(Order::where(['user_id' => $user->id, 'order_id' => $id])->first())) {
            $data = ['error' => 2, 'msg' => \Lang::get('user::default.not_access')];
            return Encryption::encryptor($data, $request->key);
        }

        if($limit)
            $data = OrderItem::where('order_id', $id)->limit($limit)->offset($offset)->get();
        else
            $data = OrderItem::where('order_id', $id)->get();

        if(count($data))
        {
            foreach($data as $item) {
                $item->image = Product::find($item->product_id)->image;
            }
            return Encryption::encryptor($data, $request->key);
        }
        $result_message = [
            'error' => 3, 'msg' => 'خالی است'
        ];
        return Encryption::encryptor($data, $request->key);
    }

    public function addToCart(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        if(!$request->product_id){
            $data = ['error' => 1, 'cart_id' => '', 'msg' => \Lang::get('order::default.product_id_needed')];
            return Encryption::encryptor($data, $request->key);
        }

        $data = array();
        $data['user_id'] = $user->id;
        $data['product_id'] = $request->product_id;
        $data['count'] = $request->count;
        $data['price'] = Product::find($request->product_id)->sell_price;

        $cart = Cart::where(['user_id' => $user->id, 'product_id' => $data['product_id']])->get()->first();
        if(count($cart)){
            $cart->count += $data['count'];
            $cart->save();
            $cart_id = $cart->id;
        }
        else
            $cart_id = Cart::create($data);

        $data = ['error' => 0, 'cart_id' => $cart_id , 'msg' => \Lang::get('order::default.add_cart')];
        return Encryption::encryptor($data, $request->key);
    }

    public function getUserCart(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $limit = $request->limit;
        $offset = $request->offset;
        $user_id = $user->id;

        DB::table('carts')
            ->where(['user_id' => $user_id])
            ->whereIn('product_id', function($query){
                $query->select('id')->from('products')->where('active', 0)->get();
            })->delete();

        if($limit)
            $data = Cart::where('user_id' , $user_id)->limit($limit)->offset($offset)->get();
        else
            $data = Cart::where('user_id' , $user_id)->get();

        if(count($data))
        {
            foreach($data as $item) {
                $item->image = Product::find($item->product_id)->image;
                $item->name = Product::find($item->product_id)->name;
            }
            return Encryption::encryptor($data, $request->key);
        }
        $data = [
            'error' => 2, 'msg' => 'سبد خرید خالی است'
        ];
        return Encryption::encryptor($data, $request->key);
    }

    public function cartCount(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $data = Cart::where(['user_id' => $user->id])->count();
        $data = [
            'error' => 0 , 'msg' => 'تعداد محصولات سبد خرید شما' , 'data' => $data
        ];
        return Encryption::encryptor($data, $request->key);
    }

    public function removeFromCart(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $id = $request->id;
        if(!count(Cart::where(['user_id' => $user->id, 'id' => $id])->first())) {
            $data = ['error' => 2, 'msg' => \Lang::get('user::default.not_access')];
            return Encryption::encryptor($data, $request->key);
        }
        $data = Cart::find($request->id);
        if(count($data))
        {
            $data->delete();
            $data = [
                'error' => 0 , 'msg' => 'از سبد خرید حذف شد'
            ];
        }
        else
        {
            $data = [
                'error' => 3 , 'msg' => 'خطایی رخ داده است'
            ];
        }
        return Encryption::encryptor($data, $request->key);
    }

    public function removeAllInCart(Request $request) {
        $user = \Auth::user();
        if(!($user->active && $user->verified)){
            $data = ['error' => 1, 'msg' => \Lang::get('user::default.not_active')];
            return Encryption::encryptor($data, $request->key);
        }
        $id = $user->id;
        Cart::where('user_id' , $id)->delete();
        $data  = [
            'error' => 0 , 'msg' => 'سبد خرید کاربر حذف شد'
        ]  ;
        return Encryption::encryptor($data, $request->key);
    }
}
