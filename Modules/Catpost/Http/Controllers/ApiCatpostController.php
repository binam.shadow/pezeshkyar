<?php
namespace Modules\Catpost\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Catpost\Entities\Catpost;


class ApiCatpostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       $mehrdad= Catpost::all();

        return $mehrdad;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $catpost = new Catpost();
        return view('catpost::create', compact('catpost'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('catpost.create') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // ...
        ]);

        Catpost::create($request->all());
        return back()
            ->with('msg', \Lang::get('catpost::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Catpost $catpost)
    {
        return view('catpost::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Catpost $catpost)
    {
        return view('catpost::edit', compact('catpost'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Catpost $catpost, Request $request)
    {
        Entrust::can('catpost.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $catpost->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Catpost $catpost)
    {
        try {
            $catpost->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('catpost.list')
                ->with('msg', \Lang::get('catpost::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function catpostList(){
          return DataTables::of(Catpost::query())
                      ->make(true);
     }
}
