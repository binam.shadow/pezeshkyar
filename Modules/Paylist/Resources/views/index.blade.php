@extends('adminbsb.layouts.master')

@section('title')
    @lang('paylist::default.list_title')
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/sweetalert/sweetalert.css') }}">
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>@lang('paylist::default.list_title')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable paylist-list" width="100%">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>@lang('paylist::default.name')</th>
                                <th>@lang('paylist::default.description')</th>
                                <th>@lang('paylist::default.bank_name')</th>
                                <th>@lang('paylist::default.dargah')</th>
                                <th>@lang('paylist::default.cat_id')</th>
                                <th>@lang('paylist::default.user_id')</th>
                                <th>@lang('paylist::default.setting')</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('adminbsb.layouts.datatable_files')
    <script type="text/javascript" src="{{ asset('backend/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var dataTable = $('.paylist-list').DataTable({
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{{ route('paylist.dataList') }}'
            },
            responsive: true,
            serverSide: true,
            columns: [
                {data: 'id', name:'id'},
                {data: 'name'},
                {data: 'description'},
                {data: 'bank_name'},
                {data: 'dargah'},
                {data: 'cat_id'},
                {data: 'user_id'},
            ],
            columnDefs: [
                {
                    targets: 6,
                    render: function(data, type, row) {
                        var btns = '';
					@permission('paylist.edit')
                        btns += '<a href="paylists/'+row.id+'/edit'+'"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="ویرایش" type="button" class="btn btn-xs btn-info waves-effect"><i class="material-icons">mode_edit</i></button></a> ';
                        btns += '<button onclick="showConfirmDelete('+row.id+')" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف" type="button" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">delete</i></button>';
					@endpermission
                        return btns;
                    }
                }
            ],
            buttons: [
                'copyHtml5', 'excelHtml5', 'print'
            ],
            language:{
                url: "{{ asset('backend/plugins/jquery-datatable/fa.json') }}"
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        function showConfirmDelete(id){
            swal({
                title: "@lang('paylist::default.are_you_sure')",
                text: "@lang('paylist::default.warning_delete_msg')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "@lang('paylist::default.cancel')",
                confirmButtonText: "@lang('paylist::default.confirm')",
                closeOnConfirm: false
            }, function (action) {
                if(action)
                    $.ajax({
                        url: "paylists/"+id,
                        //data: "_method=delete",
                        processData: false,
                        contentType: false,
                        type: 'DELETE',
                        success: function (result) {
                            if(result.status == 'success') {
                                swal("@lang('paylist::default.success_delete')", "@lang('paylist::default.success_delete_msg')", "success");
                                dataTable.ajax.reload()
                            }
                        },
                        error: function (){

                        }
                    });
            });
        }
    </script>
@endsection
