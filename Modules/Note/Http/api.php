<?php
Route::group([
 //   'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Note\Http\Controllers'
], function() {

    Route::get('gettusernotes/{postid}/{userid}', [
        'uses'	=> 'ApiNoteController@index'
    ]);
    Route::post('addnote', [
        'uses'	=> 'ApiNoteController@store'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/