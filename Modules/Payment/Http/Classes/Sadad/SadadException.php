<?php

namespace Modules\Payment\Http\Classes\Sadad;


use Modules\Payment\Http\Classes\Exceptions\BankException;

class SadadException extends BankException {}
