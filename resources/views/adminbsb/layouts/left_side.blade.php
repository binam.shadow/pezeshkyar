<!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    @if(!is_null($user->avatar))
                        <img src="{{ asset('uploads/' . $user->avatar) }}" width="48" height="48" alt="User" />
                    @else
                        <img src="{{ asset('backend/images/user.png') }}" width="48" height="48" alt="User" />
                    @endif
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $user->name }}</div>
                    <div class="email">{{ $user->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ route('admin.logout') }}"><i class="material-icons">input</i>خروج</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">@lang('admin::default.dashboard')</li>
                    <li class="active">
                        <a href="{{ route('admin.index') }}">
                            <i class="material-icons">home</i>
                            <span>@lang('admin::default.home')</span>
                        </a>
                    </li>
                    @foreach($menus as $menu)
                        @permission($menu->sub_menus->pluck('name')->toArray())
                        <li @if(in_array(Route::currentRouteName(), $menu->sub_menus->pluck('url')->toArray())) class="active" @endif>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">{{$menu->icon}}</i>
                                <span>{{$menu->display_name}} </span>
                            </a>
                            <ul class="ml-menu">
                                @if(isset($menu->sub_menus))
                                @foreach($menu->sub_menus as $sub_menu)
                                    @permission($sub_menu->name)
                                    <li @if(Route::currentRouteName() == $sub_menu->url) class="active" @endif>
                                        <a href="{{ route($sub_menu->url) }}">
                                            {{ $sub_menu->display_name }}
                                        </a>
                                    </li>
                                    @endpermission
                                @endforeach
                                @endif
                            </ul>
                        </li>
                        @endpermission
                    @endforeach
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <a href="javascript:void(0);">پنل مدیریتی @if(count($title)) {{$title->value}} @else رهام وب @endif</a> &copy; 2016 - 2017
                    
                </div>
                <div class="version">
                    <b>Version: </b> 0.0.4
                </div>
                <p class="last-login">
                    ⏰
                    <span>آخرین ورود {{$user->last_login}}</span>
                </p>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
