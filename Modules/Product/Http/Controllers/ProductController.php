<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductCategory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('product::index');
    }
    public function getChildren(Request $request)
    {
        //      (Entrust::can('product.edit') || Entrust::can('product.create')) ?: abort(403);
        $id = $request->id;
        $categories = ProductCategory::where('parent_id', $id)->get();

        $product_id = $request->product_id;

        if (isset($request->category_id)) {
            $category_id = $request->category_id;
            $category_ids = [];
            $category = ProductCategory::find($category_id);
            while (true) {
                array_push($category_ids, $category->id);
                $category = ProductCategory::find($category->parent_id);
                if (is_null($category))
                    break;
            }

//            if(is_null($product_id)) {
//                $attribute_categories = ShoppingAttributeCategory::with(['attributes' => function($query) {
//                    $query->with('productAttributes');
//                }]);
//            }
//            else {
//                $attribute_categories = ShoppingAttributeCategory::with(['attributes' => function($query) use ($product_id) {
//                    $query->with(['productAttributes'=>function($query) use($product_id){
//                        $query->where('product_id',$product_id);
//                    }]);
//                }]);
//            }
//
//            $attribute_categories = $attribute_categories->whereIn('category_id', $category_ids)
//                ->where('type', 'filter')
//                ->get();
//
//        }
//        else {
//            $attribute_categories = [];
//        }


        }
        $data = ['categories' => $categories];
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('product::create');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function add()
    {
        return view('product::add');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function postAdd(Request $request)
    {
        return $request->all();
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('product::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
