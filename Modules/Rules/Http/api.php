<?php
Route::group([
  //  'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Rules\Http\Controllers'
], function() {

    Route::get('getrule', [
        'uses'	=> 'ApiRulesController@index'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/