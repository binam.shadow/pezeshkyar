@extends('adminbsb.layouts.master')

@section('title')
    @lang('tabs::default.create')
@endsection

@section('styles')
    @include('tabs::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('tabs::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('tabs::partials._errors')
                    <form id="tabsEdit" method="post" action="{{ route('tabs.update', ['tabs' => $tabs])) }}">
                        {{ csrf_field() }}
                        @include('tabs::_form', ['saveButtonLabel'=> 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('tabs::partials._script')
@endsection
