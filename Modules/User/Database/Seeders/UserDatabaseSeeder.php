<?php

namespace Modules\User\Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
        factory(User::class, 10)->create();

        // $this->call("OthersTableSeeder");
    }
}
