<?php
Route::group([
//    'middleware' => ['auth:api', 'role:user', 'apiEncription'],
    'prefix' => 'api',
    'namespace' => 'Modules\Tabpost\Http\Controllers'
], function() {

    Route::get('tagsdetailslist/', [
        'uses'	=> 'ApiTabpostController@tagsdetailslist'
    ]);
});
/*
Route::post('api/adImageUpload', [
    'uses'	=> 'Modules\Estate\Http\Controllers\ApiEstateController@imageUpload'
]);
*/