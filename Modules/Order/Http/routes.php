<?php

Route::group([
    'middleware' => ['web', 'permission:order*'],
    'prefix' => 'admin/order',
    'namespace' => 'Modules\Order\Http\Controllers'
], function() {
    /*** Start Order ***/
    Route::get('list', [
        'as'	=> 'order.list',
        'uses'	=> 'OrderController@list'
    ]);
    Route::get('dataList', [
        'as'	=> 'order.dataList',
        'uses'	=> 'OrderController@dataList'
    ]);
    Route::get('orderItemsDataList/{id?}', [
        'as'	=> 'order_items.dataList',
        'uses'	=> 'OrderController@orderItemsDataList'
    ]);
    Route::get('show/{id?}', [
        'as'	=> 'order.show',
        'uses'	=> 'OrderController@show'
    ]);
    Route::get('print/{id?}', [
        'as'	=> 'order.print',
        'uses'	=> 'OrderController@print'
    ]);
    Route::get('delete', [
        'as'	=> 'order.delete',
        'uses'	=> 'OrderController@delete'
    ]);
    Route::get('getUserData', [
        'as'	=> 'order.getUserData',
        'uses'	=> 'OrderController@getUserData'
    ]);
});

Route::group([
    'middleware' => ['web', 'role:user'],
    'namespace' => 'Modules\Order\Http\Controllers'
], function() {

    Route::get('userOrdersList', [
        'as'	=> 'order.userOrdersList',
        'uses'	=> 'OrderController@userOrdersList'
    ]);
    Route::get('userOrdersDataList', [
        'as'	=> 'order.userOrdersDataList',
        'uses'	=> 'OrderController@userOrdersDataList'
    ]);
});
