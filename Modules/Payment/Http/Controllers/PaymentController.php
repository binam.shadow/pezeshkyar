<?php

namespace Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Gateway;
use App\Setting;

class PaymentController extends Controller {

    public function edit() {
        // Get all gateways config
        $gateways = collect(Setting::whereIn('key', ['saman', 'mellat', 'jahanpay', 'parsian', 'pasargad', 'payline', 'sadad', 'saman', 'zarinpal'])->get()->toArray());

        // Get Active gateways
        $active_gateways = Setting::where('key', 'bank_active')->get()->pluck('value')->toArray();


        // Decode mellat json
        $mellat = json_decode($gateways->where('key', 'mellat')->first()['value'], true);
        // Decode saman json
        $saman = json_decode($gateways->where('key', 'saman')->first()['value'], true);
        // Decode sadad json
        $sadad = json_decode($gateways->where('key', 'sadad')->first()['value'], true);
        // Decode parsian json
        $parsian = json_decode($gateways->where('key', 'parsian')->first()['value'], true);
        // Decode zarinpal json
        $zarinpal = json_decode($gateways->where('key', 'zarinpal')->first()['value'], true);

        return view('payment::settings', compact('mellat', 'saman', 'sadad', 'parsian', 'zarinpal', 'active_gateways'));
    }

    public function update(Request $request) {
        // Get getway from request
        $gateway = strtolower(trim($request->gateway));

        // Change gateway active or deactive
        $bank_active = ['key' => 'bank_active', 'value' => $gateway];
        if($request->active)
            Setting::updateOrCreate($bank_active, $bank_active);
        else
            Setting::where($bank_active)->delete();

        // Get config from request
        $config = json_encode($request->except('gateway', '_token', 'active'));

        // Update gateway config
        Setting::updateOrCreate(['key' => $gateway], [ 'value' => $config ]);

        // Redirect with success message
        return redirect()->route('payment.edit')
                    ->with('msg', \Lang::get('payment::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }
}
