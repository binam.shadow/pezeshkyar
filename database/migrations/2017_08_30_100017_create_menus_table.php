<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191)->nullable();
            $table->integer('parent_id')->unsigned()->default(0);
            $table->integer('parent_parent_id')->unsigned()->default(0);
            $table->string('display_name', 256)->nullable();
            $table->string('description', 256)->nullable();
            $table->string('url', 191)->nullable();
            $table->string('icon', 256)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
