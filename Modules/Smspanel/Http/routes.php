<?php

Route::group(['middleware' => 'web', 'prefix' => 'smspanels', 'namespace' => 'Modules\Smspanel\Http\Controllers'], function()
{
    Route::get('/',[
        'as'	=> 'smspanel.list',
        'uses'	=> 'SmspanelController@index'
    ]);
    Route::get('create', [
        'as'	=> 'smspanel.create',
        'uses'	=> 'SmspanelController@create'
    ]);
    Route::post('store', [
        'as'	=> 'smspanel.store',
        'uses'	=> 'SmspanelController@store'
    ]);
    Route::get('{smspanel}/edit', [
        'as'	=> 'smspanel.edit',
        'uses'	=> 'SmspanelController@edit'
    ]);
    Route::post('{smspanel}', [
        'as'	=> 'smspanel.update',
        'uses'	=> 'SmspanelController@update'
    ]);
    Route::get('/list', [
        'as'	=> 'smspanel.dataList',
        'uses'	=> 'SmspanelController@smspanelList'
    ]);
    Route::delete('{smspanel}', [
        'as'	=> 'smspanel.delete',
        'uses'	=> 'SmspanelController@destroy'
    ]);
});