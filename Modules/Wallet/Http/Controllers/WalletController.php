<?php
namespace Modules\Wallet\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use DataTables;
use Modules\Wallet\Entities\Wallet;


class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('wallet::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $wallet = new Wallet();
        return view('wallet::create', compact('wallet'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Entrust::can('wallet.create') ?: abort(403);
        $this->validate($request,[
      //      'name' => 'required',
            // ...
        ]);

        Wallet::create($request->all());
        return back()
            ->with('msg', \Lang::get('wallet::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Wallet $wallet)
    {
        return view('wallet::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Wallet $wallet)
    {
        return view('wallet::edit', compact('wallet'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Wallet $wallet, Request $request)
    {
        Entrust::can('wallet.edit') ?: abort(403);
        $this->validate($request,[
    //        'name' => 'required',
            // continue...
        ]);

        $wallet->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Wallet $wallet)
    {
        try {
            $wallet->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('wallet.list')
                ->with('msg', \Lang::get('wallet::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function walletList(){
          return DataTables::of(Wallet::query())
                      ->make(true);
     }
}
