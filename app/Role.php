<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

	protected static $logFillable = true;

    public function permissions(){
       return $this->belongsToMany(Permission::class);
    }

	public function getDescriptionForEvent(string $eventName): string {
		switch ($eventName) {
			case 'created':
				return "ثبت نقش جدید";
				break;

			case 'updated':
				return "ویرایش نقش";
				break;

			case 'deleted':
				return "حذف نقش";
				break;

			default:
				return '';
				break;
		}
    }
}
