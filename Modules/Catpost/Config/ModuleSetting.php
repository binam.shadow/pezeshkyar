<?php
/**
 * Created by PhpStorm.
 * User: Armin
 * Date: 12/03/2018
 * Time: 10:00 PM
 */
namespace Modules\Catpost\Config;

use App\Menu;
use App\Permission;
use Artisan;
use App\Role;

class ModuleSetting
{
    public $en_name = 'Catpost';
    public $fa_name = 'بیماری';
    public $fa_name_plural = 'بیماری ها';
    public $icon = 'add';

    public function setup(){
        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create_permission = Permission::updateOrCreate(['name' => 'catpost.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit_permission = Permission::updateOrCreate(['name' => 'catpost.edit'], $permissions);

        $permissions = [
            'display_name' => 'لیست ' . $this->fa_name_plural
        ];
        $list_permission = Permission::updateOrCreate(['name' => 'catpost.list'], $permissions);

        $menu1 = [
            'display_name'  => $this->fa_name,
            'icon'          => $this->icon
        ];
        $menu_parent = Menu::updateOrCreate(['name' => 'catpost'], $menu1);

        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => 'catpost.create'
        ];
        Menu::updateOrCreate(['name' => 'catpost.create'], $menu2);

        $menu3 = [
            'display_name' => 'لیست ' . $this->fa_name_plural,
            'parent_id'    => $menu_parent->id,
            'url'          => 'catpost.list'
        ];
        Menu::updateOrCreate([
            'name' => 'catpost.list'
        ], $menu3);

        $role = Role::where('name','super-admin')->first();
        if($role){
            $role->attachPermission($create_permission);
            $role->attachPermission($edit_permission);
            $role->attachPermission($list_permission);
        }

        Artisan::call('module:migrate', ['module' => 'Catpost']);

    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', 'catpost%')->delete();

        Menu::where('name', 'like', 'catpost%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => 'Catpost' ]);
    }
}