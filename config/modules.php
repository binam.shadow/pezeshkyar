<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Module Namespace
    |--------------------------------------------------------------------------
    |
    | Default module namespace.
    |
    */

    'namespace' => 'Modules',

    /*
    |--------------------------------------------------------------------------
    | Module Stubs
    |--------------------------------------------------------------------------
    |
    | Default module stubs.
    |
    */

    'stubs' => [
        'enabled' => true,
        //'path' => base_path() . '/vendor/nwidart/laravel-modules/src/Commands/stubs',
        'path' => storage_path() . '/nwidart/stubs',
        'files' => [
            'start' => 'start.php',
            'routes' => 'Http/routes.php',
            'api' => 'Http/api.php',
            // 'views/master' => 'Resources/views/layouts/master.blade.php',
            'views/index' => 'Resources/views/index.blade.php',
            'views/form' => 'Resources/views/partials/_form.blade.php',
            'views/errors' => 'Resources/views/partials/_errors.blade.php',
            'views/script' => 'Resources/views/partials/_script.blade.php',
            'views/style' => 'Resources/views/partials/_style.blade.php',
            'views/create' => 'Resources/views/create.blade.php',
            'views/edit' => 'Resources/views/edit.blade.php',
            'fa' => 'Resources/lang/fa/default.php',
            'scaffold/config' => 'Config/config.php',
            'setting/ModuleSetting' => 'Config/ModuleSetting.php',
            //'api-controller' => ['Http/Controllers/ApiModuleController.php'],
            'composer' => 'composer.json',
        ],
        'replacements' => [
            'start' => ['LOWER_NAME'],
            'routes' => ['LOWER_NAME', 'STUDLY_NAME', 'MODULE_NAMESPACE'],
            'api' => ['LOWER_NAME', 'STUDLY_NAME', 'MODULE_NAMESPACE'],
            'json' => ['LOWER_NAME', 'STUDLY_NAME', 'MODULE_NAMESPACE'],
            'views/index' => ['LOWER_NAME'],
            'views/master' => ['STUDLY_NAME', 'LOWER_NAME'],
            'views/form' => ['STUDLY_NAME', 'LOWER_NAME'],
            'views/errors' => ['STUDLY_NAME', 'LOWER_NAME'],
            'views/create' => ['STUDLY_NAME', 'LOWER_NAME'],
            'views/edit' => ['STUDLY_NAME', 'LOWER_NAME'],
            //'lang/fa' => [],
            'scaffold/config' => ['STUDLY_NAME'],
            'setting/ModuleSetting' => ['STUDLY_NAME','LOWER_NAME','MODULE_NAMESPACE'],
            //'api-controller' => ['STUDLY_NAME','CLASS','LOWER_NAME','MODULE_NAMESPACE'],
            'composer' => [
                'LOWER_NAME',
                'STUDLY_NAME',
                'VENDOR',
                'AUTHOR_NAME',
                'AUTHOR_EMAIL',
                'MODULE_NAMESPACE',
            ],
        ],
    ],
    'paths' => [
        /*
        |--------------------------------------------------------------------------
        | Modules path
        |--------------------------------------------------------------------------
        |
        | This path used for save the generated module. This path also will be added
        | automatically to list of scanned folders.
        |
        */

        'modules' => base_path('Modules'),
        /*
        |--------------------------------------------------------------------------
        | Modules assets path
        |--------------------------------------------------------------------------
        |
        | Here you may update the modules assets path.
        |
        */

        'assets' => public_path('modules'),
        /*
        |--------------------------------------------------------------------------
        | The migrations path
        |--------------------------------------------------------------------------
        |
        | Where you run 'module:publish-migration' command, where do you publish the
        | the migration files?
        |
        */

        'migration' => base_path('database/migrations'),
        /*
        |--------------------------------------------------------------------------
        | Generator path
        |--------------------------------------------------------------------------
        |
        | Here you may update the modules generator path.
        |
        */

        'generator' => [
            'assets' => 'Assets',
            'config' => 'Config',
            'command' => 'Console',
            'event' => false, //'Events',
            'listener' => false, //'Listeners',
            'migration' => 'Database/Migrations',
            'model' => 'Entities',
            'repository' => false, //'Repositories',
            'seeder' => 'Database/Seeders',
            'controller' => 'Http/Controllers',
            'filter' => 'Http/Middleware',
            'request' => 'Http/Requests',
            'provider' => 'Providers',
            'lang' => 'Resources/lang',
            'lang-fa' => 'Resources/lang/fa',
            'views' => 'Resources/views',
            'test' => 'Tests',
            'jobs' => false, //'Jobs',
            'emails' => false, //'Emails',
            'notifications' => false, //'Notifications',
            'storage' => 'Storage',
            'framework' => 'Storage/Framework',
            'cache' => 'Storage/Framework/cache',
            'sessions' => 'Storage/Framework/sessions',
            'testing' => 'Storage/Framework/testing',
            'sf-views' => 'Storage/Framework/views',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Scan Path
    |--------------------------------------------------------------------------
    |
    | Here you define which folder will be scanned. By default will scan vendor
    | directory. This is useful if you host the package in packagist website.
    |
    */

    'scan' => [
        'enabled' => false,
        'paths' => [
            base_path('vendor/*/*'),
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Composer File Template
    |--------------------------------------------------------------------------
    |
    | Here is the config for composer.json file, generated by this package
    |
    */

    'composer' => [
        'vendor' => 'nwidart',
        'author' => [
            'name' => 'Nicolas Widart',
            'email' => 'n.widart@gmail.com',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Caching
    |--------------------------------------------------------------------------
    |
    | Here is the config for setting up caching feature.
    |
    */
    'cache' => [
        'enabled' => false,
        'key' => 'laravel-modules',
        'lifetime' => 60,
    ],
    /*
    |--------------------------------------------------------------------------
    | Choose what laravel-modules will register as custom namespaces.
    | Setting one to false will require to register that part
    | in your own Service Provider class.
    |--------------------------------------------------------------------------
    */
    'register' => [
        'translations' => true,
    ],
];
