<?php
namespace Modules\Favorite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Entrust;
use Datatables;
 use Modules\Favorite\Entities\Favorite;


class ApiFavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cat = Favorite::where([
            'user_id' => 1,
        //    'parent_parent_id' => 0
        ])->get();
        return $cat;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $favorite = new Favorite();
        return view('favorite::create', compact('favorite'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    //    Entrust::can('favorite.create') ?: abort(403);
        $this->validate($request,[
           // 'name' => 'required',
            // ...
        ]);

        Favorite::create($request->all());
        return back()
            ->with('msg', \Lang::get('favorite::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Favorite $favorite)
    {
        return view('favorite::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Favorite $favorite)
    {
        return view('favorite::edit', compact('favorite'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Favorite $favorite, Request $request)
    {
        Entrust::can('favorite.edit') ?: abort(403);
        $this->validate($request,[
            'name' => 'required',
            // continue...
        ]);

        $favorite->update($request->all());

        return back()->with('msg', \Lang::get('$request::default.success_edit'))
                    ->with('msg_color', 'bg-green');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Favorite $favorite)
    {
        try {
            $favorite->delete();
            // ...
        } catch(\Exception $exception){
            dd($exception);
            redirect()->route('favorite.list')
                ->with('msg', \Lang::get('favorite::default.failure_delete'))
                ->with('msg_color', 'bg-red');
        }
        return \response(['status' => 'success']);
    }

    /**
     * Fetch data for datatables
     * @return Response
     */
     public function favoriteList(){
          return DataTables::of(Favorite::query())
                      ->make(true);
     }
}
