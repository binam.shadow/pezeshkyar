<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Modules\Tabpost\Entities\Tabpost;
use Modules\Setting\Entities\Slider;
use Modules\Tags\Entities\Tags;

class MorphServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        
        Relation::morphMap([
            'categories' => ProductCategory::class,
            'tabposts' => Tabpost::class,
            'advertises' => Tags::class,
            'sliders' => Slider::class,
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        
    }
}
