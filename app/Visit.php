<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Visit
 * @package App
 * @version September 6, 2017, 8:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsProductCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property integer count
 * @property string ips
 */
class Visit extends Model
{
    use SoftDeletes;

    public $table = 'visits';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'count',
        'ips'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'count' => 'integer',
        'ips' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
