@extends('adminbsb.layouts.master')

@section('title')
    @lang('log::default.create')
@endsection

@section('styles')
    @include('log::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('log::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('log::partials._errors')
                    <form id="LogEdit" method="post" action="{{ route('Log.update', ['Log' => $Log])) }}">
                        {{ csrf_field() }}
                        @include('log::_form', ['saveButtonLabel'=> 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('log::partials._script')
@endsection
