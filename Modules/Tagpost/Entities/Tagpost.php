<?php

namespace Modules\Tagpost\Entities;

use Illuminate\Database\Eloquent\Model;

class tagpost extends Model
{
    public $fillable = [
        'name',
        'description',
        'link',
        'tab_id',
         'image',
        // 'icon'
    ];
    public function media(){
        return $this->morphMany(Medium::class, 'mediumable');
    }

    public function notes() {
        return $this->hasMany(\Modules\Note\Entities\note::class);
    }
    public function comments() {
        return $this->hasMany(\Modules\Comment\Entities\Comment::class);
    }
}
