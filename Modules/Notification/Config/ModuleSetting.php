<?php
/**
 * Created by PhpStorm.
 * User: Armin
 * Date: 12/03/2018
 * Time: 10:00 PM
 */
namespace Modules\Notification\Config;

use App\Menu;
use App\Permission;
use Artisan;
use App\Role;

class ModuleSetting
{
    public $en_name = 'Notification';
    public $fa_name = 'نوتیفیکیشن';
    public $fa_name_plural = 'نوتیفیکیشن ها';
    public $icon = 'add';

    public function setup(){
        $permissions = [
            'display_name' => 'ایجاد ' . $this->fa_name
        ];
        $create_permission = Permission::updateOrCreate(['name' => 'notification.create'], $permissions);

        $permissions = [
            'display_name' => 'ویرایش ' . $this->fa_name
        ];
        $edit_permission = Permission::updateOrCreate(['name' => 'notification.edit'], $permissions);

        $permissions = [
            'display_name' => 'لیست ' . $this->fa_name_plural
        ];
        $list_permission = Permission::updateOrCreate(['name' => 'notification.list'], $permissions);

        $menu1 = [
            'display_name'  => $this->fa_name,
            'icon'          => $this->icon
        ];
        $menu_parent = Menu::updateOrCreate(['name' => 'notification'], $menu1);

        $menu2 = [
            'display_name' => 'ایجاد ' . $this->fa_name,
            'parent_id'    => $menu_parent->id,
            'url'          => 'notification.create'
        ];
        Menu::updateOrCreate(['name' => 'notification.create'], $menu2);

        $menu3 = [
            'display_name' => 'لیست ' . $this->fa_name_plural,
            'parent_id'    => $menu_parent->id,
            'url'          => 'notification.list'
        ];
        Menu::updateOrCreate([
            'name' => 'notification.list'
        ], $menu3);

        $role = Role::where('name','super-admin')->first();
        if($role){
            $role->attachPermission($create_permission);
            $role->attachPermission($edit_permission);
            $role->attachPermission($list_permission);
        }

        Artisan::call('module:migrate', ['module' => 'Notification']);

    }

    /**
     * @throws \Exception
     */
    public function remove(){
        Permission::where('name', 'like', 'notification%')->delete();

        Menu::where('name', 'like', 'notification%')->delete();

        Artisan::call('module:migrate-rollback', ['module' => 'Notification' ]);
    }
}