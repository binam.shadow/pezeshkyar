<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\BlogCategory;
use Entrust;
use Validator;
use File;
use DataTables;
use App\Medium;


class BlogCategoryController extends Controller
{

    public function create() {
        Entrust::can('blogcategory.create') ?: abort(403);

        return view('blog::blogcategory.create');
    }

    public function store(Request $request) {
        Entrust::can('blogcategory.create') ?: abort(403);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'description' => 'max:256',
            'image' => 'max:1024',
            'icon' => 'max:1024'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $data['name'] = $request->name;
        $data['description'] = $request->description;

        // Insert data to database
        $blog_category = BlogCategory::create($data);
        
        if($request->icon)
			Medium::make($request->icon, $blog_category->name, [
				'position' 	=> 0,
				'model' 	=> 'blog_categories',
				'model_id' 	=> $blog_category->id,
				'description'	=>  $blog_category->name
			], 'icon')->store();

		if($request->image)
			Medium::make($request->image, $blog_category->name, [
				'position' 	=> 0,
				'model' 	=> 'blog_categories',
				'model_id' 	=> $blog_category->id,
				'description'	=> $blog_category->name
			], 'image')->store();

        // Redirect back with success message
        return back()
            ->with('msg', \Lang::get('blog::default.success_create'))
            ->with('msg_color', 'bg-green');
    }

    public function list() {
        return view('blog::blogcategory.list');
    }

    public function dataList(Request $request) {
        Entrust::can('blogcategory.list') ?: abort(403);
        
        return DataTables::of(BlogCategory::query())
                        ->addColumn('icon', function ($query) {
                            return json_decode($query->icon, JSON_UNESCAPED_SLASHES);
                        })
                        ->addColumn('image', function ($query) {
                            return json_decode($query->image, JSON_UNESCAPED_SLASHES);
                        })
                        ->make(true);
    }

    public function edit($id = 0) {
        Entrust::can('blogcategory.edit') ?: abort(403);

        // Get All Parents
        $blogcategory = BlogCategory::where('id', $id)->first();
        count($blogcategory) ?: abort(404);

        return view('blog::blogcategory.edit', compact('blogcategory'));
    }

    public function update($id, Request $request) {
        Entrust::can('blogcategory.edit') ?: abort(403);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'description' => 'max:256',
            'image' => 'max:1024',
            'icon' => 'max:1024'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        // Insert data to database
        $blogcategory = BlogCategory::where('id', $id)->first();

        $data['name'] = $request->name;
        $data['description'] = $request->description;

        // If image isset -> upload
        // if ($request->image) {
        //     $data['image'] = FileManager::uploader('images/categories', $request->image);
        //     FileManager::delete($blogcategory->image);
        // }
        $data['image'] = $request->image;

        // If icon isset -> upload
        // if ($request->icon) {
        //     $data['icon'] = FileManager::uploader('images/categories', $request->icon);
        //     FileManager::delete($blogcategory->icon);
        // }
        $data['icon'] = $request->icon;

        $blogcategory->update($data);

        return redirect()->route('blogcategory.list')
            ->with('msg', \Lang::get('blog::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function delete(Request $request) {
        Entrust::can('blogcategory.edit') ?: abort(403);

        // Get blogcategory
        $blogcategory = BlogCategory::where('id', $request->id)->first();

        // Delete main blogcategory image and icon
        FileManager::delete($blogcategory->image);
        FileManager::delete($blogcategory->icon);

        // Delete main blogcategory
        $blogcategory->delete();

        // Retrun success to show success msg in front
        return ['status' => 'success'];
    }

    public function imageDelete(Request $request) {
        $id = $request->key;
        $blogcategory = BlogCategory::find($id);
        FileManager::delete($blogcategory->image);
        $blogcategory->image = null;
        $blogcategory->save();

        return ['status' => 'success'];
    }

    public function iconDelete(Request $request) {
        $id = $request->key;
        $blogcategory = BlogCategory::find($id);
        FileManager::delete($blogcategory->icon);
        $blogcategory->icon = null;
        $blogcategory->save();

        return ['status' => 'success'];
    }
}
