@extends('adminbsb.layouts.master')

@section('title')
    @lang('tags::default.create')
@endsection

@section('styles')
    @include('tags::partials._style')
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <h2>@lang('tags::default.create')</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('tags::partials._errors')
                    <form id="tagsEdit" method="post" action="{{ route('tags.update', ['tags' => $tags])) }}">
                        {{ csrf_field() }}
                        @include('tags::_form', ['saveButtonLabel'=> 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('tags::partials._script')
@endsection
