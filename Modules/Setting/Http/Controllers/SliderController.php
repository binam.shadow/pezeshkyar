<?php

namespace Modules\Setting\Http\Controllers;

use App\Classes\FileManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Setting;
use Entrust;
use File;
use Modules\Setting\Entities\Slider;
use Validator;

class SliderController extends Controller {

    public function create() {
        Entrust::can('slider.create') ?  : abort(403);

        return view('setting::slider.create');
    }

    public function store(Request $request) {
        Entrust::can('slider.create') ?: abort(403);
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:256',
            'description' => 'max:256',
            'image' => 'max:1024',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $slider = $request->all();

        if($slider['active'] == 'on')
            $slider['active'] = 1;
        else
            $slider['active'] = 0;

        $slider['image'] = $request->image;
        Slider::create($slider);

        // Redirect with success message
        return redirect()->route('slider.create')
            ->with('msg', \Lang::get('setting::default.slider_success'))
            ->with('msg_color', 'bg-green');
    }

    public function editSlider($id) {
        Entrust::can('slider.edit') ?  : abort(403);

        $slider = Slider::find($id);

        return view('setting::slider.edit', compact('slider'));
    }

    public function updateSlider($id = null, Request $request) {
        Entrust::can('slider.edit') ?  : abort(403);
        $slider = Slider::find($id);
        $slider_data = $request->except(['_token']);
        if($request->active == 'on')
            $slider_data['active'] = 1;
        else
            $slider_data['active'] = 0;

        $slider_data['image'] = $request->image;
        Slider::find($id)->update($slider_data);
        return redirect()->route('slider.list')
            ->with('msg', \Lang::get('setting::default.success_edit'))
            ->with('msg_color', 'bg-green');
    }

    public function sliderList() {
        Entrust::can('slider.list') ?  : abort(403);

        return view('setting::slider.list');
    }

    public function sliderDataList(Request $request) {
        Entrust::can('slider.list') ?  : abort(403);

        $limit = $request->length;
        $offset = $request->start;
        $order_column = $request->order[0]['column'];
        $order_dir = $request->order[0]['dir'];

        $data['draw'] =  $request->draw;
        $data['data'] = Slider::Where(function ($query) use($request) {
            $query->where("title", "LIKE", "%" . $request->search['value'] . "%")
                ->orWhere("description", "LIKE", "%" . $request->search['value'] . "%");
            })
            ->offset($offset)
            ->limit($limit)
            ->orderBy('type')
            ->orderBy($request->columns[$order_column]['data'], $order_dir)
            ->get();

        $count = Slider::count();
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;//$data['data']->count();

        return $data;
    }

    public function activate(Request $request) {
        Entrust::can('slider.edit') ?  : abort(403);

        // Set default False (if action is active set True)
        $data['active'] = $request->action;

        // Update Blog
        Slider::find($request->id)->update($data);

        // Redirect with success message
        return ['status' => 'success'];
    }

    public function delete(Request $request) {
        Entrust::can('slider.edit') ?  : abort(403);

        $slider = Slider::find($request->id);
        FileManager::delete($slider->image);
        $slider->delete();

        return ['status' => 'success'];
    }

}
