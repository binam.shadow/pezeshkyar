@extends('adminbsb.layouts.master')

@section('title')
@lang('setting::default.about_edit')
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}">
<!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="card">
			<div class="header">
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-6">
						<h2>@lang('setting::default.about_edit')</h2>
					</div>
				</div>
			</div>
			<div class="body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form id="aboutEdit" method="post" action="{{ route('about.edit') }}">
					{{ csrf_field() }}
					<div class="row clearfix">
						<div class="col-md-12">
							<div class="form-group form-float">
								<div class="form-line">
									<input class="form-control" name="name" value="{{ $about->name }}" required="" aria-required="true" type="text">
									<label class="form-label">@lang('setting::default.name') <span class="col-red">*</span></label>
								</div>
							</div>
							<div class="form-group form-float">
								<div class="form-line">
									<textarea class="form-control ckeditor" name="description" required="" aria-required="true" type="text">{{ $about->description }}</textarea>
								</div>
							</div>
							<div class="clearfix"></div>
							<style>
								.col-center{
									float: none;
									margin: 0 auto;
								}
							</style>
							<div class="row clearfix"></div>
							<div class="col-md-12 text-center">
								<p class="text-center">@lang('blog::default.feature_image')</p>
								<div class="col-sm-3 col-center text-center">
									<img style="width: 100%" id="image-show" @if($about->image == '') src="{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}" @else src="{{ asset('uploads/' . $about->image) }}" @endif>
								</div>
								<input id="image" type="hidden" name="image" @if($about->image != '') value="{{$about->image}}" @endif>
								<a href="javascript:void(0)" onclick="deleteImage('image')">حذف</a> <a class="btn btn-info" type="button" href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/about&relative_url=1&type=1&amp;popup=1&amp;field_id=image')}}')" >@lang('blog::default.choose_file')</a>
							</div>
							<div class="row clearfix"></div>
							<input type="hidden" id="temp_image" name="temp_image">
							<div class="row">
								<p class="text-right">@lang('blog::default.choose_file')</p>
								<div id="galleryBlock">
									@foreach($galleries as $gallery)
										<?php
										$rand_x = rand(0, 99999);
										?>
										<div id="imageG{{$rand_x}}" class="col-xs-6 col-md-3">
											<input name="images[]" value="{{$gallery->image}}" type="hidden">
											<a class="thumbnail"><img src="{{ url('uploads/' . $gallery->image) }}" class="img-responsive"></a>
											<p class="text-center"><a class="btn btn-danger btn-sm waves-effect" onclick="deleteGallery('{{$rand_x}}')">
													<i class="material-icons">delete_forever</i> حذف</a></p>
										</div>
									@endforeach
								</div>
								<div class="col-xs-6 col-md-3">
									<a href="javascript:open_popup('{{url('filemanager/dialog.php?fldr=images/about&relative_url=1&type=1&amp;popup=1&amp;field_id=temp_image')}}')" class="thumbnail">
										<img src="http://placehold.it/500x300&text=Add+Image" class="img-responsive">
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group form-float">
						<div class="switch">
							<label>@lang('setting::default.active') <input name="active" @if($about->active) checked="" @endif type="checkbox"><span class="lever switch-col-blue"></span> @lang('setting::default.deactive')</label>
						</div>
					</div>
					<div class="form-group form-float">
						<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect">@lang('setting::default.about_edit')</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/bootstrap-fileinput/js/locales/fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jquery-validation/localization/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/config.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/ckeditor/lang/fa.js') }}"></script>
<script type="text/javascript">
	function responsive_filemanager_callback(field_id){
		if(field_id != 'temp_image') {
			// show image in src
			$('#'+field_id+'-show').attr('src', '{{ url('uploads') }}/' + $('#'+field_id).val());
		} else {
			// add new box with random id
			var rand_x = Math.floor((Math.random() * 99999) + 1);
			var new_box = '<div id="imageG'+rand_x+'" class="col-xs-6 col-md-3">'+
					'<input name="images[]" type="hidden" value="'+$('#'+field_id).val()+'">'+
					'<a class="thumbnail"><img src="{{ url('uploads') }}/' + $('#'+field_id).val()+'" class="img-responsive"></a><p class="text-center"><a class="btn btn-danger btn-sm waves-effect" onclick="deleteGallery('+rand_x+')"><i class="material-icons">delete_forever</i> حذف</a></p></div>';
			$('#galleryBlock').append(new_box);
		}
	}
	function open_popup(url){var w=880;var h=570;var l=Math.floor((screen.width-w)/2);var t=Math.floor((screen.height-h)/2);var win=window.open(url,'ResponsiveFilemanager',"scrollbars=1,width="+w+",height="+h+",top="+t+",left="+l);}
	function deleteImage(field_id) {
		$('#'+field_id+'-show').attr('src', '{{ asset('backend/plugins/bootstrap-fileinput/img/default-avatar.png') }}');
		$('#'+field_id).val('');
	}
	function deleteGallery(box_id) {
		$("#imageG"+box_id).remove();
	}
</script>

<script type="text/javascript">
$("#gallery").fileinput({
	showUpload: false,
	overwriteInitial: false,
	@if(count($galleries))
	initialPreview: [
		@foreach($galleries as $gallery)
				"{{ asset('uploads/' . $gallery->image) }}",
		@endforeach
	],
	@endif
	initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
	language: 'fa',
	initialPreviewFileType: 'image',
	maxFileCount: 5,
	allowedFileExtensions: ["jpg", "png", "gif"]
});
$('#aboutEdit').validate({
	rules: {
		'name': {
			required: true
		},
		'description': {
			required: true
		}
	}
});
</script>
@endsection
