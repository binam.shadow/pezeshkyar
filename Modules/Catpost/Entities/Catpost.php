<?php

namespace Modules\Catpost\Entities;

use Illuminate\Database\Eloquent\Model;

class catpost extends Model
{
    protected $fillable = [
        'name',
        'description',
        'link',
        'cat_id',
    ];
    public function media(){
        return $this->morphMany(Medium::class, 'mediumable');
    }
    public function post()
    {
        return $this->belongsTo(\Modules\Product\Entities\ProductCategory::class);
    }
}
