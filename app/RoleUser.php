<?php

namespace App;



/**
 * Class Order
 * @package Modules\Product\Entities
 * @version September 6, 2017, 8:28 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection carts
 * @property \Illuminate\Database\Eloquent\Collection OrderItem
 * @property \Illuminate\Database\Eloquent\Collection orderItemsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsFeatures
 * @property \Illuminate\Database\Eloquent\Collection productsProductCategories
 * @property \Illuminate\Database\Eloquent\Collection wishlists
 * @property integer user_id
 * @property integer city_id
 * @property integer province_id
 * @property string address
 * @property string postal_code
 * @property bigInteger total_price
 * @property string status
 */
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{

    public $table = 'role_user';

}
